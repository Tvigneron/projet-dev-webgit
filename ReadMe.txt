Procedure de deployment:


Windows :
    Commencer par installer Xampp ( via ce lien https://www.apachefriends.org/fr/index.html )
    installer "ubuntu 20.4.*" Via le windows store
    Lancez "ubuntu 20.4.*"
    Rentrez les commandes :
        sudo apt install git-all
        cd [le fichier de destination pour la creation]
        git clone https://gitlab.com/Tvigneron/projet-dev-web.gitgit.git
        sudo apt install php7.4
        sudo php composer-setup.php --install-dir=/usr/local/bin --filename=composer
    Allumez Xampp et activez MySQL
    Ensuite placez vous dans le dossier en question à l'aide de votre terminal et tapez la commande suivante :
        php bin/console server:run
    A l'aide d'un autre terminal placé dans le meme repertoire lancez les commandes :
        php bin/console doctrine:database:create --if-not-exists
        php bin/console doctrine:schema:update --force
        php bin/console doctrine:fixtures:load

    Vous pouvez maintenant acceder au site Web en rentrant dans votre navigateur l'adresse suivante:
        127.0.0.1:8000
    
Linux 
    Commencer par installer Xampp ( via ce lien https://www.apachefriends.org/fr/index.html )
    Ouvrez un terminal et rentrez les commandes :
        sudo apt install git-all
        cd [le fichier de destination pour la creation]
        git clone https://gitlab.com/Tvigneron/projet-dev-web.gitgit.git
        sudo apt install php7.4
        sudo php composer-setup.php --install-dir=/usr/local/bin --filename=composer
    Allumez Xampp et activez MySQL
    Ensuite placez vous dans le dossier en question à l'aide de votre terminal et tapez la commande suivante :
        php bin/console server:run
    A l'aide d'un autre terminal placé dans le meme repertoire lancez les commandes :
        php bin/console doctrine:database:create --if-not-exists
        php bin/console doctrine:schema:update --force
        php bin/console doctrine:fixtures:load

    Vous pouvez maintenant acceder au site Web en rentrant dans votre navigateur l'adresse suivante:
        127.0.0.1:8000

MAC 
    Commencer par installer Xampp ( via ce lien https://www.apachefriends.org/fr/index.html )
    Ouvrez un terminal et rentrez les commandes :
        /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install.sh)" brew doctor
        brew install git

// Nous n'avons pas de mac et nous ne connaissons pas la syntaxe de mac
// Nous sommes desolés mais il nous est impossible de decrire une installation via Mac

    Vous pouvez maintenant acceder au site Web en rentrant dans votre navigateur l'adresse suivante:
        127.0.0.1:8000