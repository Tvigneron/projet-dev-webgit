<?php

namespace App\Repository;

use App\Entity\Evenement;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Evenement|null find($id, $lockMode = null, $lockVersion = null)
 * @method Evenement|null findOneBy(array $criteria, array $orderBy = null)
 * @method Evenement[]    findAll()
 * @method Evenement[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class EvenementRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Evenement::class);
    }

    // /**
    //  * @return Evenement[] Returns an array of Evenement objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('e.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Evenement
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
    /**
    * @return Evenement[]
    */
    public function getEvenementNonExpire()
    {
    $qb = $this->createQueryBuilder('e')
    ->where('e.date > :date')
    ->setParameter('date', new \DateTime())
    ->orderBy('e.date', 'DESC');
    return $qb->getQuery()->getResult();
    }


    /**
    * @return Evenement[]
    */
    public function searchEvenements($data)
    {
     
        $qb = $this->createQueryBuilder('e');
       // dd([$data['titre'],$data['adresse'],$data['ville'],$data['description']] );
        if($data['titre'] != null){
            $qb->where('e.titre = :titre')
            ->setParameter('titre', $data['titre']);
        }
        if($data['adresse'] != null){
            $qb->andWhere('e.adresse = :adresse')
            ->setParameter('adresse', $data['adresse']);
        }
        if($data['ville'] != null){
            $qb->andWhere('e.ville = :ville')
            ->setParameter('ville', $data['ville']);
        }
        if($data['description'] != null){
            $qb->andWhere('e.description = :description')
            ->setParameter('description', $data['description']);
        }
        if($data['jeux'] != 'Tous'){
            $qb->andWhere('e.jeux = :jeux')
            ->setParameter('jeux', $data['jeux']);
        }
        $qb->orderBy('e.date', 'DESC');
        
        return $qb->getQuery()->getResult();
    }
}
