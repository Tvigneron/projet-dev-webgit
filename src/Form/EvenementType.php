<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use App\Entity\Evenement;

use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Form\Extension\Core\Type\DateType;

class EvenementType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
    $builder
        ->add('titre', TextType::class, ['constraints' => [new NotBlank(), new Length(['max' => 255]),] ] )
        ->add('adresse', TextType::class, ['constraints' => [new NotBlank(),new Length(['max' => 255]),] ] )
        ->add('ville', TextType::class, ['constraints' => [new NotBlank(),new Length(['max' => 255]),] ] )
        ->add('description', TextType::class)
        ->add('date', DateType::class, [
            'widget' => 'single_text',
        
            // prevents rendering it as type="date", to avoid HTML5 date pickers
            'html5' => false,
        
            // adds a class that can be selected in JavaScript
            'attr' => ['class' => 'js-datepicker'],
        ])
        ->add('jeux', ChoiceType::class,[
            'choices'  => [
            'Cs Go' => 'Cs Go',
            'Overwatch' => 'Overwatch',
            'League of Legends' => 'League of Legends',
            'Rocket League' => 'Rocket League',

        ],
        ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            // Configure your form options here
        ]);
    }
}
