<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use App\Entity\Evenement;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Length;


class SearchType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('titre', TextType::class, [
                'required' => false
            ]
        )
        ->add('adresse', TextType::class, 
            ['required' => false], 
        )
        ->add('ville', TextType::class,
            ['required' => false], 
        )
        ->add('description', TextType::class,
            ['required' => false], 
        )
        ->add('jeux', ChoiceType::class,[
            'choices'  => [
            'Tous' => 'Tous',
            'Cs Go' => 'Cs Go',
            'Overwatch' => 'Overwatch',
            'League of Legends' => 'League of Legends',
            'Rocket League' => 'Rocket League',

        ],
        ])
        ->add('recherche', SubmitType::class,);
    }

}
