<?php

namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;


class UserFixtures extends Fixture
{
    private $passwordEncoder;
    public function __construct(UserPasswordEncoderInterface $passwordEncoder)
    {
        $this->passwordEncoder = $passwordEncoder;
    }

    public function load(ObjectManager $manager)
    {
        $num = "06688414501";
        $user = new User();
        $user->setEmail("user1@user.fr")
        ->setNom("Dupont")
        ->setPrenom("Benjamin")
        ->setNumero($num)
        ->setAge(20)
        ->setPassword($this->passwordEncoder->encodePassword($user, 'user1'));
        
        $user1 = new User();
        $user1->setEmail("user2@user.fr")
        ->setNom("Alexandre")
        ->setPrenom("Lebleu")
        ->setNumero($num)
        ->setAge(19)
        ->setPassword($this->passwordEncoder->encodePassword($user, 'user2'));

        $user2 = new User();
        $user2->setEmail("user3@user.fr")
        ->setNom("Clemence")
        ->setPrenom("Dugazon")
        ->setNumero($num)
        ->setAge(25)
        ->setPassword($this->passwordEncoder->encodePassword($user, 'user3'));

        $manager->persist($user);

        $admin = new User();
        $admin->setEmail("admin@admin.fr")
        ->setNom("Bourdon")
        ->setPrenom("Laura")
        ->setNumero($num)
        ->setAge(20)
        ->setPassword($this->passwordEncoder->encodePassword($user, 'admin'));
        $admin->setRoles(['ROLE_ADMIN']);
        $manager->persist($admin);
        $manager->persist($user);
        $manager->persist($user1);
        $manager->persist($user2);
        $manager->flush();

    }
}