<?php
namespace App\DataFixtures;

use App\Entity\Evenement;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class EvenementFixtures extends Fixture implements FixtureInterface
{
    /**
     * @param ObjectManager $manager
     *
     * @return void
     */
    public function load(ObjectManager $manager) : void
    {

        $Evenement1 = new Evenement();
        $description = "Lorem ipsum dolor sit amet, consectetur adipiscing ";
        $Evenement1->setAdresse(" 143 boulevard des paves")
        ->setVille('Nantes')
        ->setDescription($description)
        ->setjeux('Rocket League')
        ->setDate(new \DateTime('+30 days'))
        ->setTitre("Tournois Rocket League");

        $Evenement2 = new Evenement();
        $Evenement2->setAdresse(" 1 Rue des pavés")
        ->setVille('Blois')
        ->setDescription($description)
        ->setjeux('League of Legends')
        ->setDate(new \DateTime('+90 days'))
        ->setTitre("Lan Rocket League");

        $Evenement3 = new Evenement();
        $Evenement3->setAdresse(" 15 Rue de la Chocolaterie")
        ->setVille('Blois')
        ->setDescription($description)
        ->setjeux('League of Legends')
        ->setDate(new \DateTime('+120 days'))
        ->setTitre("Projet Tournois League of Legends");
        
        $Evenement4 = new Evenement();
        $Evenement4->setAdresse("29 Rue du Pont Volan")
        ->setVille('Tours')
        ->setDescription($description)
        ->setjeux('Cs Go')
        ->setDate(new \DateTime('+50 days'))
        ->setTitre("Projet Tournois League of Legends");

        $Evenement5 = new Evenement();
        $Evenement5->setAdresse("3 Rue du Clos Courtel")
        ->setVille('Rennes')
        ->setDescription($description)
        ->setjeux('OverWatch')
        ->setDate(new \DateTime('+120 days'))
        ->setTitre("Tournois OverWatch");

        $manager->persist($Evenement1);
        $manager->persist($Evenement2);
        $manager->persist($Evenement3);
        $manager->persist($Evenement4);
        $manager->persist($Evenement5);
        $manager->flush();
    }
}