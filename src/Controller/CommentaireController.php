<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\ORM\EntityManagerInterface;
use App\Entity\Evenement;
use App\Entity\Commentaire;
use App\Entity\User;
use App\Form\EvenementType;
use App\Form\CommentaireType;
use App\Repository\EvenementRepository;
use Symfony\Component\HttpFoundation\Request;
use App\Controller\SecurityController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

class CommentaireController extends AbstractController
{
     /**
     * Supprime le commentaire en question .
     *  @Route("/{_locale}/deletecomm/{id}", name="commentaire.delete",  requirements={"id" = "\d+"})
     * @return Response
     */
    public function delete(Request $request, EntityManagerInterface $em , int $id)
    {
        $commentaire = $this->getDoctrine()->getRepository(Commentaire::class)->find($id);
        $utilisateur = $commentaire->getUtilisateur();
        $evenement = $commentaire->getEvenement();
        $evenement->removeCommentaire($commentaire);
        $utilisateur->removeCommentaire($commentaire);
        $em->remove($commentaire);
        $em->flush();
        return $this->redirectToRoute('evenement.show',['id' => $evenement->getId()]);

    }
}
