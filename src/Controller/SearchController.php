<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\ORM\EntityManagerInterface;
use App\Entity\Evenement;
use App\Form\SearchType;
use App\Repository\EvenementRepository;
use Symfony\Component\HttpFoundation\Request;
use App\Controller\SecurityController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

class SearchController extends AbstractController
{
    /**
     * @Route("/{_locale}/search", name="search")
     * Permet de generer une recherche selon les criteres fournis dans la section rechercher un evenement
     */
    public function searchEvenement(Request $request, EvenementRepository $evenementRepository): Response
    {
    $evenements = [];
    $form = $this->createForm(SearchType::class,);
    $form->handleRequest($request);
    if ($form->isSubmitted() && $form->isValid()) {
        $data  = $form->getData();
        $evenements = $evenementRepository->searchEvenements($data);
    }
    return $this->render('evenement/rechercheprecise.html.twig', [
        'form' => $form->createView(),
        'evenements' => $evenements,
    
    ]);
    }
}
