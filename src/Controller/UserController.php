<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\ORM\EntityManagerInterface;
use App\Entity\Evenement;
use App\Entity\User;
use App\Form\EvenementType;
use App\Form\CreationCompteType;
use App\Repository\EvenementRepository;
use Symfony\Component\HttpFoundation\Request;
use App\Controller\SecurityController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserController extends AbstractController
{
    /**
     * @Route("/{_locale}/participe/{id}", name="participe.user",  requirements={"id" = "\d+"})
     * permet d'inscrire un utilisateur a un evenement
     */
    public function participe(Request $request, EntityManagerInterface $em , int $id)
    {
        $evenement = $this->getDoctrine()->getRepository(Evenement::class)->find($id);
        $utilisateur = $this->get('security.token_storage')->getToken()->getUser();
        $utilisateur->addEvenement($evenement);
        $evenement->addParticipe($utilisateur);
        $em->persist($utilisateur);
        $em->persist($evenement);
        $em->flush();
        return $this->redirectToRoute('evenement.list');

    }
    /**
     * Chercher et afficher le compte de l'utilisateur.
     * @Route("/{_locale}/user", name="user.show")
     * Permet de generer la vue mon compte en affichant le compte de l'utilisateur / ses commentaires / ses evenements
     * @return Response
     */
    public function show() : Response
    {
        $user = $this->get('security.token_storage')->getToken()->getUser();
        $evenement = $this->get('security.token_storage')->getToken()->getUser()->getEvenements();
        $commentaires = $this->get('security.token_storage')->getToken()->getUser()->getCommentaires();
        return $this->render('user/show.html.twig', ['user' => $user,'evenements' => $evenement,'commentaires' => $commentaires, ]);
    }
  

    /**
     * Chercher et afficher le compte de l'utilisateur.
     *  @Route("/{_locale}/desincrire/{id}", name="user.sedesinscrire",  requirements={"id" = "\d+"})
     * permet de desinscrire un utilisateur a un evenement
     * @return Response
     */
    public function sedesinscrire(Request $request, EntityManagerInterface $em , int $id)
    {
        $evenement = $this->getDoctrine()->getRepository(Evenement::class)->find($id);
        $utilisateur = $this->get('security.token_storage')->getToken()->getUser();
        $utilisateur->removeEvenement($evenement);
        $evenement->removeParticipe($utilisateur);
        $em->persist($utilisateur);
        $em->persist($evenement);
        $em->flush();
        return $this->redirectToRoute('user.show', );

    }
    /**
    * @Route("/{_locale}/creercompte", name="user.creercompte")
    * Permet de creer un compte utilisateur     
    */
    public function creation(Request $request,EntityManagerInterface $em,UserPasswordEncoderInterface $passwordEncoder){
        $user = new User();
        $form = $this->createForm(CreationCompteType::class, $user );
        
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $user->setPassword($passwordEncoder->encodePassword($user, $user->getPassword()) );
            $em->persist($user);
            $em->flush();
            return $this->redirectToRoute('app_login');
        }
        return $this->render('user/create.html.twig', ['form' => $form->createView(),]);
        
    }

}
