<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\ORM\EntityManagerInterface;
use App\Entity\Evenement;
use App\Entity\Commentaire;
use App\Entity\User;
use App\Form\EvenementType;
use App\Form\CommentaireType;
use App\Repository\EvenementRepository;
use Symfony\Component\HttpFoundation\Request;
use App\Controller\SecurityController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
/* ....
* @Route("/{_locale}/")
*/ 
class EvenementController extends AbstractController
{
    /**
     * Lister tous les evenements ayant une date > a celle actuelle.
     * @Route("/{_locale}/", name="evenement.list")
     * @return Response
     */
    public function list() : Response
    {
    
    $evenement = $this->getDoctrine()->getRepository(Evenement::class)->getEvenementNonExpire();
    return $this->render('evenement/list.html.twig', ['evenements' => $evenement, ]);
    }
    /**
     * Affiche l'evenement en question.
     * @Route("/{_locale}/evenement/{id}", name="evenement.show", requirements={"id" = "\d+"})
     * @param Evenement $evenement
     * @return Response
     */
    public function show(Request $request ,EntityManagerInterface $em, Evenement $evenement, int $id) : Response
    {
    $form = $this->createForm(CommentaireType::class);
    $form->handleRequest($request);
    $utilisateur = $this->get('security.token_storage')->getToken()->getUser();
    if ($form->isSubmitted() && $form->isValid()) {
        $data  = $form->getData();
        $utilisateur = $this->get('security.token_storage')->getToken()->getUser();
        $commentaire = new Commentaire();
        $commentaire->setDescription($data['Commentaire']);
        $commentaire->setUtilisateur($this->get('security.token_storage')->getToken()->getUser());
        $commentaire->setEvenement($evenement);
        $utilisateur->addCommentaire($commentaire);
        $evenement->addCommentaire($commentaire);
        $em->persist($evenement);
        $em->persist($commentaire);
        $em->persist($utilisateur);
        $em->flush();
    }
    return $this->render('evenement/show.html.twig', ['form' => $form->createView(),'evenement' => $evenement,'commentaires'=>$evenement->getCommentaires(), 'user' =>$utilisateur]);    
    }
    /**
     * Require ROLE_ADMIN for *every* controller method in this class.
     *
     * @IsGranted("ROLE_ADMIN")
     * Créer un nouvel evenement.
     * @Route("/{_locale}/nouvel-evenement", name="evenement.create")
     * @param Request $request
     * @param EntityManagerInterface $em
     * @return RedirectResponse|Response
     */
    public function create(Request $request, EntityManagerInterface $em) : Response
    {
    $evenement = new Evenement();
    $form = $this->createForm(EvenementType::class, $evenement);
    $form->handleRequest($request);
    if ($form->isSubmitted() && $form->isValid()) {
        $em->persist($evenement);
        $em->flush();
        return $this->redirectToRoute('evenement.list');
    }
    return $this->render('evenement/create.html.twig', ['form' => $form->createView(),]);
    }

    /**
     * Éditer un evenement.
     * @IsGranted("ROLE_ADMIN")
     * @Route("/{_locale}/evenement/{id}/edit", name="evenement.edit", requirements={"id" = "\d+"})
     * @param Request $request
     * @param EntityManagerInterface $em
     * @return RedirectResponse|Response
     */
    public function edit(Request $request, Evenement $evenement, EntityManagerInterface $em) : Response
    {
    $form = $this->createForm(EvenementType::class, $evenement);
    $form->handleRequest($request);
    if ($form->isSubmitted() && $form->isValid()) {
        $em->flush();
        return $this->redirectToRoute('evenement.list');
    }
    return $this->render('evenement/edit.html.twig', ['form' => $form->createView(),]);
    }
     /**
     * Supprimer un evenement.
     * @IsGranted("ROLE_ADMIN")
     * @Route("/{_locale}/evenement/{id}/delete", name="evenement.delete",requirements={"id" = "\d+"})
     * @param Request $request
     * @param Evenement $evenement
     * @param EntityManagerInterface $em
     * @return Response
     */
    public function delete(Request $request, Evenement $evenement, EntityManagerInterface $em) : Response
    {
        $form = $this->createFormBuilder()
        ->setAction($this->generateUrl('evenement.delete', ['id' => $evenement->getId()]))
        ->getForm();
        $form->handleRequest($request);
        if ( ! $form->isSubmitted() || ! $form->isValid()) {
            return $this->render('evenement/delete.html.twig', ['evenement' => $evenement,'form' => $form->createView(),]);
        }
        $em = $this->getDoctrine()->getManager();
        $em->remove($evenement);
        $em->flush();
        return $this->redirectToRoute('evenement.list');
    }
}