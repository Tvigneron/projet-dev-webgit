<?php

declare(strict_types=1);

namespace PackageVersions;

use Composer\InstalledVersions;
use OutOfBoundsException;

class_exists(InstalledVersions::class);

/**
 * This class is generated by composer/package-versions-deprecated, specifically by
 * @see \PackageVersions\Installer
 *
 * This file is overwritten at every run of `composer install` or `composer update`.
 *
 * @deprecated in favor of the Composer\InstalledVersions class provided by Composer 2. Require composer-runtime-api:^2 to ensure it is present.
 */
final class Versions
{
    /**
     * @deprecated please use {@see self::rootPackageName()} instead.
     *             This constant will be removed in version 2.0.0.
     */
    const ROOT_PACKAGE_NAME = '__root__';

    /**
     * Array of all available composer packages.
     * Dont read this array from your calling code, but use the \PackageVersions\Versions::getVersion() method instead.
     *
     * @var array<string, string>
     * @internal
     */
    const VERSIONS          = array (
  'behat/transliterator' => 'v1.3.0@3c4ec1d77c3d05caa1f0bf8fb3aae4845005c7fc',
  'composer/package-versions-deprecated' => '1.11.99.1@7413f0b55a051e89485c5cb9f765fe24bb02a7b6',
  'doctrine/annotations' => '1.12.1@b17c5014ef81d212ac539f07a1001832df1b6d3b',
  'doctrine/cache' => '1.10.2@13e3381b25847283a91948d04640543941309727',
  'doctrine/collections' => '1.6.7@55f8b799269a1a472457bd1a41b4f379d4cfba4a',
  'doctrine/common' => '2.13.3@f3812c026e557892c34ef37f6ab808a6b567da7f',
  'doctrine/dbal' => '2.12.1@adce7a954a1c2f14f85e94aed90c8489af204086',
  'doctrine/doctrine-bundle' => '1.11.2@28101e20776d8fa20a00b54947fbae2db0d09103',
  'doctrine/doctrine-cache-bundle' => '1.4.0@6bee2f9b339847e8a984427353670bad4e7bdccb',
  'doctrine/doctrine-migrations-bundle' => '3.0.2@b8de89fe811e62f1dea8cf9aafda0ea45ca6f1f3',
  'doctrine/event-manager' => '1.1.1@41370af6a30faa9dc0368c4a6814d596e81aba7f',
  'doctrine/inflector' => '1.4.3@4650c8b30c753a76bf44fb2ed00117d6f367490c',
  'doctrine/instantiator' => '1.4.0@d56bf6102915de5702778fe20f2de3b2fe570b5b',
  'doctrine/lexer' => '1.2.1@e864bbf5904cb8f5bb334f99209b48018522f042',
  'doctrine/migrations' => '3.0.1@69eaf2ca5bc48357b43ddbdc31ccdffc0e2a0882',
  'doctrine/orm' => '2.7.5@01187c9260cd085529ddd1273665217cae659640',
  'doctrine/persistence' => '1.3.8@7a6eac9fb6f61bba91328f15aa7547f4806ca288',
  'doctrine/reflection' => '1.2.2@fa587178be682efe90d005e3a322590d6ebb59a5',
  'easycorp/easyadmin-bundle' => 'v2.3.2@cf48c275e2b235898f225fb53e613d9228144ace',
  'egulias/email-validator' => '3.1.0@62c3b73c581c834885acf6e120b412b76acc495a',
  'fig/link-util' => '1.1.2@5d7b8d04ed3393b4b59968ca1e906fb7186d81e8',
  'gedmo/doctrine-extensions' => 'v2.4.42@b6c4442b4f32ce05673fbdf1fa4a2d5e315cc0a4',
  'jdorn/sql-formatter' => 'v1.2.17@64990d96e0959dff8e059dfcdc1af130728d92bc',
  'laminas/laminas-code' => '4.0.0@28a6d70ea8b8bca687d7163300e611ae33baf82a',
  'laminas/laminas-eventmanager' => '3.3.1@966c859b67867b179fde1eff0cd38df51472ce4a',
  'laminas/laminas-zendframework-bridge' => '1.2.0@6cccbddfcfc742eb02158d6137ca5687d92cee32',
  'monolog/monolog' => '1.26.0@2209ddd84e7ef1256b7af205d0717fb62cfc9c33',
  'ocramius/proxy-manager' => '2.11.1@96bb91b7b52324080accf1d0137f491ff378ecf1',
  'pagerfanta/pagerfanta' => 'v2.7.1@630f38d57c86b67565b644db9d270ffb6d67123f',
  'phpdocumentor/reflection-common' => '2.2.0@1d01c49d4ed62f25aa84a747ad35d5a16924662b',
  'phpdocumentor/reflection-docblock' => '5.2.2@069a785b2141f5bcf49f3e353548dc1cce6df556',
  'phpdocumentor/type-resolver' => '1.4.0@6a467b8989322d92aa1c8bf2bebcc6e5c2ba55c0',
  'psr/cache' => '1.0.1@d11b50ad223250cf17b86e38383413f5a6764bf8',
  'psr/container' => '1.1.1@8622567409010282b7aeebe4bb841fe98b58dcaf',
  'psr/link' => '1.0.0@eea8e8662d5cd3ae4517c9b864493f59fca95562',
  'psr/log' => '1.1.3@0f73288fd15629204f9d42b7055f72dacbe811fc',
  'psr/simple-cache' => '1.0.1@408d5eafb83c57f6365a3ca330ff23aa4a5fa39b',
  'sensio/framework-extra-bundle' => 'v5.3.1@5f75c4658b03301cba17baa15a840b57b72b4262',
  'stof/doctrine-extensions-bundle' => 'v1.3.0@46db71ec7ffee9122eca3cdddd4ef8d84bae269c',
  'swiftmailer/swiftmailer' => 'v6.2.7@15f7faf8508e04471f666633addacf54c0ab5933',
  'symfony/asset' => 'v4.2.12@82f5349982967842aeb7d2f8e876ac900be24f14',
  'symfony/cache' => 'v4.2.12@28286385757fa08c249254541ad814a8ccaff0f7',
  'symfony/config' => 'v4.2.12@637588756df1fb1c801833aead54a7153967c0cb',
  'symfony/console' => 'v4.2.12@fc2e274aade6567a750551942094b2145ade9b6c',
  'symfony/contracts' => 'v1.1.10@011c20407c4b99d454f44021d023fb39ce23b73d',
  'symfony/debug' => 'v4.2.12@f832ecacfb00688b498eedf9b10784e401d969a6',
  'symfony/dependency-injection' => 'v4.2.12@39fe71bc481483f255e388a658c8f1104fec037e',
  'symfony/deprecation-contracts' => 'v2.2.0@5fa56b4074d1ae755beb55617ddafe6f5d78f665',
  'symfony/doctrine-bridge' => 'v4.2.12@5e8d62453eda38a523a12cc918031cc8f48e4b65',
  'symfony/dotenv' => 'v4.2.12@6163f061011009655da1bc615b38941bc460ef1b',
  'symfony/event-dispatcher' => 'v4.2.12@852548c7c704f14d2f6700c8d872a05bd2028732',
  'symfony/expression-language' => 'v4.2.12@ea23981c1dee4f2f901fce8345d34614237d57ca',
  'symfony/filesystem' => 'v4.2.12@1bd7aed2932cedd1c2c6cc925831dd8d57ea14bf',
  'symfony/finder' => 'v4.2.12@cecff7164790b0cd72be2ed20e9591d7140715e0',
  'symfony/flex' => 'v1.12.2@e472606b4b3173564f0edbca8f5d32b52fc4f2c9',
  'symfony/form' => 'v4.2.12@c694ef3befb1e4bba58c33522533ecf1e11fd470',
  'symfony/framework-bundle' => 'v4.2.12@ac12bd9f104bb2dc319b09426e767ecfa95688da',
  'symfony/http-foundation' => 'v4.2.12@2ae778ff4a1f8baba7724db1ca977ada3b796749',
  'symfony/http-kernel' => 'v4.2.12@8a7c5ef599466af6e972c705507f815df9c490ae',
  'symfony/inflector' => 'v4.2.12@275e54941a4f17a471c68d2a00e2513fc1fd4a78',
  'symfony/intl' => 'v4.2.12@94aed330eda5cfe171bbd596d898c065e94c132f',
  'symfony/monolog-bridge' => 'v4.2.12@41b01701e23016b920638ed551c53f077daacefd',
  'symfony/monolog-bundle' => 'v3.6.0@e495f5c7e4e672ffef4357d4a4d85f010802f940',
  'symfony/options-resolver' => 'v4.2.12@eda69aac1ea1f97a594dd9a5c64b7ff73a37ade2',
  'symfony/polyfill-intl-icu' => 'v1.22.1@af1842919c7e7364aaaa2798b29839e3ba168588',
  'symfony/polyfill-intl-idn' => 'v1.22.1@2d63434d922daf7da8dd863e7907e67ee3031483',
  'symfony/polyfill-intl-normalizer' => 'v1.22.1@43a0283138253ed1d48d352ab6d0bdb3f809f248',
  'symfony/polyfill-mbstring' => 'v1.22.1@5232de97ee3b75b0360528dae24e73db49566ab1',
  'symfony/polyfill-php72' => 'v1.22.1@cc6e6f9b39fe8075b3dabfbaf5b5f645ae1340c9',
  'symfony/process' => 'v4.2.12@808a4be7e0dd7fcb6a2b1ed2ba22dd581402c5e2',
  'symfony/property-access' => 'v4.2.12@c3532a4bdb785446970148da68e03dc11514e256',
  'symfony/property-info' => 'v4.2.12@c5d4e006eb3fb386c5b68cd3b1dbb3f0cc6516df',
  'symfony/proxy-manager-bridge' => 'v4.2.12@848855d1cea8c038806b744f5b0e436ec4d73ced',
  'symfony/routing' => 'v4.2.12@1174ae15f862a0f2d481c29ba172a70b208c9561',
  'symfony/security-bundle' => 'v4.2.12@d731987aabf6b28b86faeb158973b0d0c22a0a09',
  'symfony/security-core' => 'v4.2.12@3ec42b5dbeee143715da686539751ea762dd8564',
  'symfony/security-csrf' => 'v4.2.12@ff004ea4d215fd4a740f6d6ca9643ff92326c16c',
  'symfony/security-guard' => 'v4.2.12@ef6d700e6be1ca75bc2788068c62506ab11461bd',
  'symfony/security-http' => 'v4.2.12@8024422eeaca7b0b33ce2900b2f75e20259de7aa',
  'symfony/serializer' => 'v4.2.12@8a2974c10e8eb8eb2d36a28c1bd68eb0b411cc60',
  'symfony/stopwatch' => 'v4.2.12@b1a5f646d56a3290230dbc8edf2a0d62cda23f67',
  'symfony/swiftmailer-bundle' => 'v3.3.1@defa9bdfc0191ed70b389cb93c550c6c82cf1745',
  'symfony/translation' => 'v4.2.12@4b84015894d980745b510ba90492722cafe2f90f',
  'symfony/twig-bridge' => 'v4.2.12@708a993aaf3b979738d6e0a12bf157f02fc94998',
  'symfony/twig-bundle' => 'v4.2.12@db06490aeabba37dfc55a53fbf901c75e0d4f7b0',
  'symfony/validator' => 'v4.2.12@7b4485db55b7ea1a0d13d126c2781313017f815f',
  'symfony/var-exporter' => 'v4.2.12@f5be0592bb191debd278cf7e16413df0c978de8f',
  'symfony/web-link' => 'v4.2.12@47b8188b4bb8d24eef0bb287b0737d5b84a6cab8',
  'symfony/yaml' => 'v4.2.12@9468fef6f1c740b96935e9578560a9e9189ca154',
  'twig/twig' => 'v2.14.4@0b4ba691fb99ec7952d25deb36c0a83061b93bbf',
  'webimpress/safe-writer' => '2.1.0@5cfafdec5873c389036f14bf832a5efc9390dcdd',
  'webmozart/assert' => '1.10.0@6964c76c7804814a842473e0c8fd15bab0f18e25',
  'doctrine/data-fixtures' => '1.5.0@51d3d4880d28951fff42a635a2389f8c63baddc5',
  'doctrine/doctrine-fixtures-bundle' => '3.0.4@0438f8dd0a21bc5325c6be3ae0a09131815e10d4',
  'nikic/php-parser' => 'v4.10.4@c6d052fc58cb876152f89f532b95a8d7907e7f0e',
  'symfony/browser-kit' => 'v4.2.12@3659f10d13d270b77506006bdf9250cac9268156',
  'symfony/css-selector' => 'v4.2.12@48eddf66950fa57996e1be4a55916d65c10c604a',
  'symfony/debug-bundle' => 'v4.2.12@7ad133ba7c5c932bca671d118aa634cd77ebb39f',
  'symfony/dom-crawler' => 'v4.2.12@ba1da8fb10291714b8db153fcf7ac515e1a217bb',
  'symfony/maker-bundle' => 'v1.30.2@a395a85aa4ded6c1fa3da118d60329b64b6c2acd',
  'symfony/phpunit-bridge' => 'v5.2.4@9d85d900c1afe29138a0d5854505eb684bc3ac6d',
  'symfony/var-dumper' => 'v4.2.12@4e18e041a477edbb8c54e053f179672f9413816c',
  'symfony/web-profiler-bundle' => 'v4.2.12@e7b916235f8a1d33010ab707fb08943cf8573c1e',
  'symfony/web-server-bundle' => 'v4.2.12@91945ba7f59f2a4b4194f018da9d7aaedaf88418',
  'paragonie/random_compat' => '2.*@',
  'symfony/polyfill-ctype' => '*@',
  'symfony/polyfill-iconv' => '*@',
  'symfony/polyfill-php71' => '*@',
  'symfony/polyfill-php70' => '*@',
  'symfony/polyfill-php56' => '*@',
  '__root__' => '1.0.0+no-version-set@',
);

    private function __construct()
    {
    }

    /**
     * @psalm-pure
     *
     * @psalm-suppress ImpureMethodCall we know that {@see InstalledVersions} interaction does not
     *                                  cause any side effects here.
     */
    public static function rootPackageName() : string
    {
        if (!class_exists(InstalledVersions::class, false) || !InstalledVersions::getRawData()) {
            return self::ROOT_PACKAGE_NAME;
        }

        return InstalledVersions::getRootPackage()['name'];
    }

    /**
     * @throws OutOfBoundsException If a version cannot be located.
     *
     * @psalm-param key-of<self::VERSIONS> $packageName
     * @psalm-pure
     *
     * @psalm-suppress ImpureMethodCall we know that {@see InstalledVersions} interaction does not
     *                                  cause any side effects here.
     */
    public static function getVersion(string $packageName): string
    {
        if (class_exists(InstalledVersions::class, false) && InstalledVersions::getRawData()) {
            return InstalledVersions::getPrettyVersion($packageName)
                . '@' . InstalledVersions::getReference($packageName);
        }

        if (isset(self::VERSIONS[$packageName])) {
            return self::VERSIONS[$packageName];
        }

        throw new OutOfBoundsException(
            'Required package "' . $packageName . '" is not installed: check your ./vendor/composer/installed.json and/or ./composer.lock files'
        );
    }
}
