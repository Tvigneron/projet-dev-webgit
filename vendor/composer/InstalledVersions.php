<?php











namespace Composer;

use Composer\Autoload\ClassLoader;
use Composer\Semver\VersionParser;






class InstalledVersions
{
private static $installed = array (
  'root' => 
  array (
    'pretty_version' => '1.0.0+no-version-set',
    'version' => '1.0.0.0',
    'aliases' => 
    array (
    ),
    'reference' => NULL,
    'name' => '__root__',
  ),
  'versions' => 
  array (
    '__root__' => 
    array (
      'pretty_version' => '1.0.0+no-version-set',
      'version' => '1.0.0.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'behat/transliterator' => 
    array (
      'pretty_version' => 'v1.3.0',
      'version' => '1.3.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '3c4ec1d77c3d05caa1f0bf8fb3aae4845005c7fc',
    ),
    'composer/package-versions-deprecated' => 
    array (
      'pretty_version' => '1.11.99.1',
      'version' => '1.11.99.1',
      'aliases' => 
      array (
      ),
      'reference' => '7413f0b55a051e89485c5cb9f765fe24bb02a7b6',
    ),
    'doctrine/annotations' => 
    array (
      'pretty_version' => '1.12.1',
      'version' => '1.12.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'b17c5014ef81d212ac539f07a1001832df1b6d3b',
    ),
    'doctrine/cache' => 
    array (
      'pretty_version' => '1.10.2',
      'version' => '1.10.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '13e3381b25847283a91948d04640543941309727',
    ),
    'doctrine/collections' => 
    array (
      'pretty_version' => '1.6.7',
      'version' => '1.6.7.0',
      'aliases' => 
      array (
      ),
      'reference' => '55f8b799269a1a472457bd1a41b4f379d4cfba4a',
    ),
    'doctrine/common' => 
    array (
      'pretty_version' => '2.13.3',
      'version' => '2.13.3.0',
      'aliases' => 
      array (
      ),
      'reference' => 'f3812c026e557892c34ef37f6ab808a6b567da7f',
    ),
    'doctrine/data-fixtures' => 
    array (
      'pretty_version' => '1.5.0',
      'version' => '1.5.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '51d3d4880d28951fff42a635a2389f8c63baddc5',
    ),
    'doctrine/dbal' => 
    array (
      'pretty_version' => '2.12.1',
      'version' => '2.12.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'adce7a954a1c2f14f85e94aed90c8489af204086',
    ),
    'doctrine/doctrine-bundle' => 
    array (
      'pretty_version' => '1.11.2',
      'version' => '1.11.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '28101e20776d8fa20a00b54947fbae2db0d09103',
    ),
    'doctrine/doctrine-cache-bundle' => 
    array (
      'pretty_version' => '1.4.0',
      'version' => '1.4.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '6bee2f9b339847e8a984427353670bad4e7bdccb',
    ),
    'doctrine/doctrine-fixtures-bundle' => 
    array (
      'pretty_version' => '3.0.4',
      'version' => '3.0.4.0',
      'aliases' => 
      array (
      ),
      'reference' => '0438f8dd0a21bc5325c6be3ae0a09131815e10d4',
    ),
    'doctrine/doctrine-migrations-bundle' => 
    array (
      'pretty_version' => '3.0.2',
      'version' => '3.0.2.0',
      'aliases' => 
      array (
      ),
      'reference' => 'b8de89fe811e62f1dea8cf9aafda0ea45ca6f1f3',
    ),
    'doctrine/event-manager' => 
    array (
      'pretty_version' => '1.1.1',
      'version' => '1.1.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '41370af6a30faa9dc0368c4a6814d596e81aba7f',
    ),
    'doctrine/inflector' => 
    array (
      'pretty_version' => '1.4.3',
      'version' => '1.4.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '4650c8b30c753a76bf44fb2ed00117d6f367490c',
    ),
    'doctrine/instantiator' => 
    array (
      'pretty_version' => '1.4.0',
      'version' => '1.4.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'd56bf6102915de5702778fe20f2de3b2fe570b5b',
    ),
    'doctrine/lexer' => 
    array (
      'pretty_version' => '1.2.1',
      'version' => '1.2.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'e864bbf5904cb8f5bb334f99209b48018522f042',
    ),
    'doctrine/migrations' => 
    array (
      'pretty_version' => '3.0.1',
      'version' => '3.0.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '69eaf2ca5bc48357b43ddbdc31ccdffc0e2a0882',
    ),
    'doctrine/orm' => 
    array (
      'pretty_version' => '2.7.5',
      'version' => '2.7.5.0',
      'aliases' => 
      array (
      ),
      'reference' => '01187c9260cd085529ddd1273665217cae659640',
    ),
    'doctrine/persistence' => 
    array (
      'pretty_version' => '1.3.8',
      'version' => '1.3.8.0',
      'aliases' => 
      array (
      ),
      'reference' => '7a6eac9fb6f61bba91328f15aa7547f4806ca288',
    ),
    'doctrine/reflection' => 
    array (
      'pretty_version' => '1.2.2',
      'version' => '1.2.2.0',
      'aliases' => 
      array (
      ),
      'reference' => 'fa587178be682efe90d005e3a322590d6ebb59a5',
    ),
    'easycorp/easyadmin-bundle' => 
    array (
      'pretty_version' => 'v2.3.2',
      'version' => '2.3.2.0',
      'aliases' => 
      array (
      ),
      'reference' => 'cf48c275e2b235898f225fb53e613d9228144ace',
    ),
    'egulias/email-validator' => 
    array (
      'pretty_version' => '3.1.0',
      'version' => '3.1.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '62c3b73c581c834885acf6e120b412b76acc495a',
    ),
    'fig/link-util' => 
    array (
      'pretty_version' => '1.1.2',
      'version' => '1.1.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '5d7b8d04ed3393b4b59968ca1e906fb7186d81e8',
    ),
    'gedmo/doctrine-extensions' => 
    array (
      'pretty_version' => 'v2.4.42',
      'version' => '2.4.42.0',
      'aliases' => 
      array (
      ),
      'reference' => 'b6c4442b4f32ce05673fbdf1fa4a2d5e315cc0a4',
    ),
    'jdorn/sql-formatter' => 
    array (
      'pretty_version' => 'v1.2.17',
      'version' => '1.2.17.0',
      'aliases' => 
      array (
      ),
      'reference' => '64990d96e0959dff8e059dfcdc1af130728d92bc',
    ),
    'laminas/laminas-code' => 
    array (
      'pretty_version' => '4.0.0',
      'version' => '4.0.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '28a6d70ea8b8bca687d7163300e611ae33baf82a',
    ),
    'laminas/laminas-eventmanager' => 
    array (
      'pretty_version' => '3.3.1',
      'version' => '3.3.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '966c859b67867b179fde1eff0cd38df51472ce4a',
    ),
    'laminas/laminas-zendframework-bridge' => 
    array (
      'pretty_version' => '1.2.0',
      'version' => '1.2.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '6cccbddfcfc742eb02158d6137ca5687d92cee32',
    ),
    'monolog/monolog' => 
    array (
      'pretty_version' => '1.26.0',
      'version' => '1.26.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '2209ddd84e7ef1256b7af205d0717fb62cfc9c33',
    ),
    'nikic/php-parser' => 
    array (
      'pretty_version' => 'v4.10.4',
      'version' => '4.10.4.0',
      'aliases' => 
      array (
      ),
      'reference' => 'c6d052fc58cb876152f89f532b95a8d7907e7f0e',
    ),
    'ocramius/package-versions' => 
    array (
      'replaced' => 
      array (
        0 => '1.11.99',
      ),
    ),
    'ocramius/proxy-manager' => 
    array (
      'pretty_version' => '2.11.1',
      'version' => '2.11.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '96bb91b7b52324080accf1d0137f491ff378ecf1',
    ),
    'pagerfanta/core' => 
    array (
      'replaced' => 
      array (
        0 => 'v2.7.1',
      ),
    ),
    'pagerfanta/doctrine-collections-adapter' => 
    array (
      'replaced' => 
      array (
        0 => 'v2.7.1',
      ),
    ),
    'pagerfanta/doctrine-dbal-adapter' => 
    array (
      'replaced' => 
      array (
        0 => 'v2.7.1',
      ),
    ),
    'pagerfanta/doctrine-mongodb-odm-adapter' => 
    array (
      'replaced' => 
      array (
        0 => 'v2.7.1',
      ),
    ),
    'pagerfanta/doctrine-orm-adapter' => 
    array (
      'replaced' => 
      array (
        0 => 'v2.7.1',
      ),
    ),
    'pagerfanta/doctrine-phpcr-odm-adapter' => 
    array (
      'replaced' => 
      array (
        0 => 'v2.7.1',
      ),
    ),
    'pagerfanta/elastica-adapter' => 
    array (
      'replaced' => 
      array (
        0 => 'v2.7.1',
      ),
    ),
    'pagerfanta/pagerfanta' => 
    array (
      'pretty_version' => 'v2.7.1',
      'version' => '2.7.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '630f38d57c86b67565b644db9d270ffb6d67123f',
    ),
    'pagerfanta/solarium-adapter' => 
    array (
      'replaced' => 
      array (
        0 => 'v2.7.1',
      ),
    ),
    'pagerfanta/twig' => 
    array (
      'replaced' => 
      array (
        0 => 'v2.7.1',
      ),
    ),
    'paragonie/random_compat' => 
    array (
      'replaced' => 
      array (
        0 => '2.*',
      ),
    ),
    'phpdocumentor/reflection-common' => 
    array (
      'pretty_version' => '2.2.0',
      'version' => '2.2.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '1d01c49d4ed62f25aa84a747ad35d5a16924662b',
    ),
    'phpdocumentor/reflection-docblock' => 
    array (
      'pretty_version' => '5.2.2',
      'version' => '5.2.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '069a785b2141f5bcf49f3e353548dc1cce6df556',
    ),
    'phpdocumentor/type-resolver' => 
    array (
      'pretty_version' => '1.4.0',
      'version' => '1.4.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '6a467b8989322d92aa1c8bf2bebcc6e5c2ba55c0',
    ),
    'psr/cache' => 
    array (
      'pretty_version' => '1.0.1',
      'version' => '1.0.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'd11b50ad223250cf17b86e38383413f5a6764bf8',
    ),
    'psr/cache-implementation' => 
    array (
      'provided' => 
      array (
        0 => '1.0',
      ),
    ),
    'psr/container' => 
    array (
      'pretty_version' => '1.1.1',
      'version' => '1.1.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '8622567409010282b7aeebe4bb841fe98b58dcaf',
    ),
    'psr/container-implementation' => 
    array (
      'provided' => 
      array (
        0 => '1.0',
      ),
    ),
    'psr/link' => 
    array (
      'pretty_version' => '1.0.0',
      'version' => '1.0.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'eea8e8662d5cd3ae4517c9b864493f59fca95562',
    ),
    'psr/link-implementation' => 
    array (
      'provided' => 
      array (
        0 => '1.0',
      ),
    ),
    'psr/log' => 
    array (
      'pretty_version' => '1.1.3',
      'version' => '1.1.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '0f73288fd15629204f9d42b7055f72dacbe811fc',
    ),
    'psr/log-implementation' => 
    array (
      'provided' => 
      array (
        0 => '1.0.0',
        1 => '1.0',
      ),
    ),
    'psr/simple-cache' => 
    array (
      'pretty_version' => '1.0.1',
      'version' => '1.0.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '408d5eafb83c57f6365a3ca330ff23aa4a5fa39b',
    ),
    'psr/simple-cache-implementation' => 
    array (
      'provided' => 
      array (
        0 => '1.0',
      ),
    ),
    'sensio/framework-extra-bundle' => 
    array (
      'pretty_version' => 'v5.3.1',
      'version' => '5.3.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '5f75c4658b03301cba17baa15a840b57b72b4262',
    ),
    'stof/doctrine-extensions-bundle' => 
    array (
      'pretty_version' => 'v1.3.0',
      'version' => '1.3.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '46db71ec7ffee9122eca3cdddd4ef8d84bae269c',
    ),
    'swiftmailer/swiftmailer' => 
    array (
      'pretty_version' => 'v6.2.7',
      'version' => '6.2.7.0',
      'aliases' => 
      array (
      ),
      'reference' => '15f7faf8508e04471f666633addacf54c0ab5933',
    ),
    'symfony/asset' => 
    array (
      'pretty_version' => 'v4.2.12',
      'version' => '4.2.12.0',
      'aliases' => 
      array (
      ),
      'reference' => '82f5349982967842aeb7d2f8e876ac900be24f14',
    ),
    'symfony/browser-kit' => 
    array (
      'pretty_version' => 'v4.2.12',
      'version' => '4.2.12.0',
      'aliases' => 
      array (
      ),
      'reference' => '3659f10d13d270b77506006bdf9250cac9268156',
    ),
    'symfony/cache' => 
    array (
      'pretty_version' => 'v4.2.12',
      'version' => '4.2.12.0',
      'aliases' => 
      array (
      ),
      'reference' => '28286385757fa08c249254541ad814a8ccaff0f7',
    ),
    'symfony/cache-contracts' => 
    array (
      'replaced' => 
      array (
        0 => 'v1.1.10',
      ),
    ),
    'symfony/cache-implementation' => 
    array (
      'provided' => 
      array (
        0 => '1.0',
      ),
    ),
    'symfony/config' => 
    array (
      'pretty_version' => 'v4.2.12',
      'version' => '4.2.12.0',
      'aliases' => 
      array (
      ),
      'reference' => '637588756df1fb1c801833aead54a7153967c0cb',
    ),
    'symfony/console' => 
    array (
      'pretty_version' => 'v4.2.12',
      'version' => '4.2.12.0',
      'aliases' => 
      array (
      ),
      'reference' => 'fc2e274aade6567a750551942094b2145ade9b6c',
    ),
    'symfony/contracts' => 
    array (
      'pretty_version' => 'v1.1.10',
      'version' => '1.1.10.0',
      'aliases' => 
      array (
      ),
      'reference' => '011c20407c4b99d454f44021d023fb39ce23b73d',
    ),
    'symfony/css-selector' => 
    array (
      'pretty_version' => 'v4.2.12',
      'version' => '4.2.12.0',
      'aliases' => 
      array (
      ),
      'reference' => '48eddf66950fa57996e1be4a55916d65c10c604a',
    ),
    'symfony/debug' => 
    array (
      'pretty_version' => 'v4.2.12',
      'version' => '4.2.12.0',
      'aliases' => 
      array (
      ),
      'reference' => 'f832ecacfb00688b498eedf9b10784e401d969a6',
    ),
    'symfony/debug-bundle' => 
    array (
      'pretty_version' => 'v4.2.12',
      'version' => '4.2.12.0',
      'aliases' => 
      array (
      ),
      'reference' => '7ad133ba7c5c932bca671d118aa634cd77ebb39f',
    ),
    'symfony/dependency-injection' => 
    array (
      'pretty_version' => 'v4.2.12',
      'version' => '4.2.12.0',
      'aliases' => 
      array (
      ),
      'reference' => '39fe71bc481483f255e388a658c8f1104fec037e',
    ),
    'symfony/deprecation-contracts' => 
    array (
      'pretty_version' => 'v2.2.0',
      'version' => '2.2.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '5fa56b4074d1ae755beb55617ddafe6f5d78f665',
    ),
    'symfony/doctrine-bridge' => 
    array (
      'pretty_version' => 'v4.2.12',
      'version' => '4.2.12.0',
      'aliases' => 
      array (
      ),
      'reference' => '5e8d62453eda38a523a12cc918031cc8f48e4b65',
    ),
    'symfony/dom-crawler' => 
    array (
      'pretty_version' => 'v4.2.12',
      'version' => '4.2.12.0',
      'aliases' => 
      array (
      ),
      'reference' => 'ba1da8fb10291714b8db153fcf7ac515e1a217bb',
    ),
    'symfony/dotenv' => 
    array (
      'pretty_version' => 'v4.2.12',
      'version' => '4.2.12.0',
      'aliases' => 
      array (
      ),
      'reference' => '6163f061011009655da1bc615b38941bc460ef1b',
    ),
    'symfony/event-dispatcher' => 
    array (
      'pretty_version' => 'v4.2.12',
      'version' => '4.2.12.0',
      'aliases' => 
      array (
      ),
      'reference' => '852548c7c704f14d2f6700c8d872a05bd2028732',
    ),
    'symfony/event-dispatcher-contracts' => 
    array (
      'replaced' => 
      array (
        0 => 'v1.1.10',
      ),
    ),
    'symfony/expression-language' => 
    array (
      'pretty_version' => 'v4.2.12',
      'version' => '4.2.12.0',
      'aliases' => 
      array (
      ),
      'reference' => 'ea23981c1dee4f2f901fce8345d34614237d57ca',
    ),
    'symfony/filesystem' => 
    array (
      'pretty_version' => 'v4.2.12',
      'version' => '4.2.12.0',
      'aliases' => 
      array (
      ),
      'reference' => '1bd7aed2932cedd1c2c6cc925831dd8d57ea14bf',
    ),
    'symfony/finder' => 
    array (
      'pretty_version' => 'v4.2.12',
      'version' => '4.2.12.0',
      'aliases' => 
      array (
      ),
      'reference' => 'cecff7164790b0cd72be2ed20e9591d7140715e0',
    ),
    'symfony/flex' => 
    array (
      'pretty_version' => 'v1.12.2',
      'version' => '1.12.2.0',
      'aliases' => 
      array (
      ),
      'reference' => 'e472606b4b3173564f0edbca8f5d32b52fc4f2c9',
    ),
    'symfony/form' => 
    array (
      'pretty_version' => 'v4.2.12',
      'version' => '4.2.12.0',
      'aliases' => 
      array (
      ),
      'reference' => 'c694ef3befb1e4bba58c33522533ecf1e11fd470',
    ),
    'symfony/framework-bundle' => 
    array (
      'pretty_version' => 'v4.2.12',
      'version' => '4.2.12.0',
      'aliases' => 
      array (
      ),
      'reference' => 'ac12bd9f104bb2dc319b09426e767ecfa95688da',
    ),
    'symfony/http-client-contracts' => 
    array (
      'replaced' => 
      array (
        0 => 'v1.1.10',
      ),
    ),
    'symfony/http-foundation' => 
    array (
      'pretty_version' => 'v4.2.12',
      'version' => '4.2.12.0',
      'aliases' => 
      array (
      ),
      'reference' => '2ae778ff4a1f8baba7724db1ca977ada3b796749',
    ),
    'symfony/http-kernel' => 
    array (
      'pretty_version' => 'v4.2.12',
      'version' => '4.2.12.0',
      'aliases' => 
      array (
      ),
      'reference' => '8a7c5ef599466af6e972c705507f815df9c490ae',
    ),
    'symfony/inflector' => 
    array (
      'pretty_version' => 'v4.2.12',
      'version' => '4.2.12.0',
      'aliases' => 
      array (
      ),
      'reference' => '275e54941a4f17a471c68d2a00e2513fc1fd4a78',
    ),
    'symfony/intl' => 
    array (
      'pretty_version' => 'v4.2.12',
      'version' => '4.2.12.0',
      'aliases' => 
      array (
      ),
      'reference' => '94aed330eda5cfe171bbd596d898c065e94c132f',
    ),
    'symfony/maker-bundle' => 
    array (
      'pretty_version' => 'v1.30.2',
      'version' => '1.30.2.0',
      'aliases' => 
      array (
      ),
      'reference' => 'a395a85aa4ded6c1fa3da118d60329b64b6c2acd',
    ),
    'symfony/monolog-bridge' => 
    array (
      'pretty_version' => 'v4.2.12',
      'version' => '4.2.12.0',
      'aliases' => 
      array (
      ),
      'reference' => '41b01701e23016b920638ed551c53f077daacefd',
    ),
    'symfony/monolog-bundle' => 
    array (
      'pretty_version' => 'v3.6.0',
      'version' => '3.6.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'e495f5c7e4e672ffef4357d4a4d85f010802f940',
    ),
    'symfony/options-resolver' => 
    array (
      'pretty_version' => 'v4.2.12',
      'version' => '4.2.12.0',
      'aliases' => 
      array (
      ),
      'reference' => 'eda69aac1ea1f97a594dd9a5c64b7ff73a37ade2',
    ),
    'symfony/phpunit-bridge' => 
    array (
      'pretty_version' => 'v5.2.4',
      'version' => '5.2.4.0',
      'aliases' => 
      array (
      ),
      'reference' => '9d85d900c1afe29138a0d5854505eb684bc3ac6d',
    ),
    'symfony/polyfill-ctype' => 
    array (
      'replaced' => 
      array (
        0 => '*',
      ),
    ),
    'symfony/polyfill-iconv' => 
    array (
      'replaced' => 
      array (
        0 => '*',
      ),
    ),
    'symfony/polyfill-intl-icu' => 
    array (
      'pretty_version' => 'v1.22.1',
      'version' => '1.22.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'af1842919c7e7364aaaa2798b29839e3ba168588',
    ),
    'symfony/polyfill-intl-idn' => 
    array (
      'pretty_version' => 'v1.22.1',
      'version' => '1.22.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '2d63434d922daf7da8dd863e7907e67ee3031483',
    ),
    'symfony/polyfill-intl-normalizer' => 
    array (
      'pretty_version' => 'v1.22.1',
      'version' => '1.22.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '43a0283138253ed1d48d352ab6d0bdb3f809f248',
    ),
    'symfony/polyfill-mbstring' => 
    array (
      'pretty_version' => 'v1.22.1',
      'version' => '1.22.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '5232de97ee3b75b0360528dae24e73db49566ab1',
    ),
    'symfony/polyfill-php56' => 
    array (
      'replaced' => 
      array (
        0 => '*',
      ),
    ),
    'symfony/polyfill-php70' => 
    array (
      'replaced' => 
      array (
        0 => '*',
      ),
    ),
    'symfony/polyfill-php71' => 
    array (
      'replaced' => 
      array (
        0 => '*',
      ),
    ),
    'symfony/polyfill-php72' => 
    array (
      'pretty_version' => 'v1.22.1',
      'version' => '1.22.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'cc6e6f9b39fe8075b3dabfbaf5b5f645ae1340c9',
    ),
    'symfony/process' => 
    array (
      'pretty_version' => 'v4.2.12',
      'version' => '4.2.12.0',
      'aliases' => 
      array (
      ),
      'reference' => '808a4be7e0dd7fcb6a2b1ed2ba22dd581402c5e2',
    ),
    'symfony/property-access' => 
    array (
      'pretty_version' => 'v4.2.12',
      'version' => '4.2.12.0',
      'aliases' => 
      array (
      ),
      'reference' => 'c3532a4bdb785446970148da68e03dc11514e256',
    ),
    'symfony/property-info' => 
    array (
      'pretty_version' => 'v4.2.12',
      'version' => '4.2.12.0',
      'aliases' => 
      array (
      ),
      'reference' => 'c5d4e006eb3fb386c5b68cd3b1dbb3f0cc6516df',
    ),
    'symfony/proxy-manager-bridge' => 
    array (
      'pretty_version' => 'v4.2.12',
      'version' => '4.2.12.0',
      'aliases' => 
      array (
      ),
      'reference' => '848855d1cea8c038806b744f5b0e436ec4d73ced',
    ),
    'symfony/routing' => 
    array (
      'pretty_version' => 'v4.2.12',
      'version' => '4.2.12.0',
      'aliases' => 
      array (
      ),
      'reference' => '1174ae15f862a0f2d481c29ba172a70b208c9561',
    ),
    'symfony/security-bundle' => 
    array (
      'pretty_version' => 'v4.2.12',
      'version' => '4.2.12.0',
      'aliases' => 
      array (
      ),
      'reference' => 'd731987aabf6b28b86faeb158973b0d0c22a0a09',
    ),
    'symfony/security-core' => 
    array (
      'pretty_version' => 'v4.2.12',
      'version' => '4.2.12.0',
      'aliases' => 
      array (
      ),
      'reference' => '3ec42b5dbeee143715da686539751ea762dd8564',
    ),
    'symfony/security-csrf' => 
    array (
      'pretty_version' => 'v4.2.12',
      'version' => '4.2.12.0',
      'aliases' => 
      array (
      ),
      'reference' => 'ff004ea4d215fd4a740f6d6ca9643ff92326c16c',
    ),
    'symfony/security-guard' => 
    array (
      'pretty_version' => 'v4.2.12',
      'version' => '4.2.12.0',
      'aliases' => 
      array (
      ),
      'reference' => 'ef6d700e6be1ca75bc2788068c62506ab11461bd',
    ),
    'symfony/security-http' => 
    array (
      'pretty_version' => 'v4.2.12',
      'version' => '4.2.12.0',
      'aliases' => 
      array (
      ),
      'reference' => '8024422eeaca7b0b33ce2900b2f75e20259de7aa',
    ),
    'symfony/serializer' => 
    array (
      'pretty_version' => 'v4.2.12',
      'version' => '4.2.12.0',
      'aliases' => 
      array (
      ),
      'reference' => '8a2974c10e8eb8eb2d36a28c1bd68eb0b411cc60',
    ),
    'symfony/service-contracts' => 
    array (
      'replaced' => 
      array (
        0 => 'v1.1.10',
      ),
    ),
    'symfony/service-implementation' => 
    array (
      'provided' => 
      array (
        0 => '1.0',
      ),
    ),
    'symfony/stopwatch' => 
    array (
      'pretty_version' => 'v4.2.12',
      'version' => '4.2.12.0',
      'aliases' => 
      array (
      ),
      'reference' => 'b1a5f646d56a3290230dbc8edf2a0d62cda23f67',
    ),
    'symfony/swiftmailer-bundle' => 
    array (
      'pretty_version' => 'v3.3.1',
      'version' => '3.3.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'defa9bdfc0191ed70b389cb93c550c6c82cf1745',
    ),
    'symfony/translation' => 
    array (
      'pretty_version' => 'v4.2.12',
      'version' => '4.2.12.0',
      'aliases' => 
      array (
      ),
      'reference' => '4b84015894d980745b510ba90492722cafe2f90f',
    ),
    'symfony/translation-contracts' => 
    array (
      'replaced' => 
      array (
        0 => 'v1.1.10',
      ),
    ),
    'symfony/translation-implementation' => 
    array (
      'provided' => 
      array (
        0 => '1.0',
      ),
    ),
    'symfony/twig-bridge' => 
    array (
      'pretty_version' => 'v4.2.12',
      'version' => '4.2.12.0',
      'aliases' => 
      array (
      ),
      'reference' => '708a993aaf3b979738d6e0a12bf157f02fc94998',
    ),
    'symfony/twig-bundle' => 
    array (
      'pretty_version' => 'v4.2.12',
      'version' => '4.2.12.0',
      'aliases' => 
      array (
      ),
      'reference' => 'db06490aeabba37dfc55a53fbf901c75e0d4f7b0',
    ),
    'symfony/validator' => 
    array (
      'pretty_version' => 'v4.2.12',
      'version' => '4.2.12.0',
      'aliases' => 
      array (
      ),
      'reference' => '7b4485db55b7ea1a0d13d126c2781313017f815f',
    ),
    'symfony/var-dumper' => 
    array (
      'pretty_version' => 'v4.2.12',
      'version' => '4.2.12.0',
      'aliases' => 
      array (
      ),
      'reference' => '4e18e041a477edbb8c54e053f179672f9413816c',
    ),
    'symfony/var-exporter' => 
    array (
      'pretty_version' => 'v4.2.12',
      'version' => '4.2.12.0',
      'aliases' => 
      array (
      ),
      'reference' => 'f5be0592bb191debd278cf7e16413df0c978de8f',
    ),
    'symfony/web-link' => 
    array (
      'pretty_version' => 'v4.2.12',
      'version' => '4.2.12.0',
      'aliases' => 
      array (
      ),
      'reference' => '47b8188b4bb8d24eef0bb287b0737d5b84a6cab8',
    ),
    'symfony/web-profiler-bundle' => 
    array (
      'pretty_version' => 'v4.2.12',
      'version' => '4.2.12.0',
      'aliases' => 
      array (
      ),
      'reference' => 'e7b916235f8a1d33010ab707fb08943cf8573c1e',
    ),
    'symfony/web-server-bundle' => 
    array (
      'pretty_version' => 'v4.2.12',
      'version' => '4.2.12.0',
      'aliases' => 
      array (
      ),
      'reference' => '91945ba7f59f2a4b4194f018da9d7aaedaf88418',
    ),
    'symfony/yaml' => 
    array (
      'pretty_version' => 'v4.2.12',
      'version' => '4.2.12.0',
      'aliases' => 
      array (
      ),
      'reference' => '9468fef6f1c740b96935e9578560a9e9189ca154',
    ),
    'twig/twig' => 
    array (
      'pretty_version' => 'v2.14.4',
      'version' => '2.14.4.0',
      'aliases' => 
      array (
      ),
      'reference' => '0b4ba691fb99ec7952d25deb36c0a83061b93bbf',
    ),
    'webimpress/safe-writer' => 
    array (
      'pretty_version' => '2.1.0',
      'version' => '2.1.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '5cfafdec5873c389036f14bf832a5efc9390dcdd',
    ),
    'webmozart/assert' => 
    array (
      'pretty_version' => '1.10.0',
      'version' => '1.10.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '6964c76c7804814a842473e0c8fd15bab0f18e25',
    ),
    'zendframework/zend-code' => 
    array (
      'replaced' => 
      array (
        0 => '4.0.0',
      ),
    ),
    'zendframework/zend-eventmanager' => 
    array (
      'replaced' => 
      array (
        0 => '^3.2.1',
      ),
    ),
  ),
);
private static $canGetVendors;
private static $installedByVendor = array();







public static function getInstalledPackages()
{
$packages = array();
foreach (self::getInstalled() as $installed) {
$packages[] = array_keys($installed['versions']);
}


if (1 === \count($packages)) {
return $packages[0];
}

return array_keys(array_flip(\call_user_func_array('array_merge', $packages)));
}









public static function isInstalled($packageName)
{
foreach (self::getInstalled() as $installed) {
if (isset($installed['versions'][$packageName])) {
return true;
}
}

return false;
}














public static function satisfies(VersionParser $parser, $packageName, $constraint)
{
$constraint = $parser->parseConstraints($constraint);
$provided = $parser->parseConstraints(self::getVersionRanges($packageName));

return $provided->matches($constraint);
}










public static function getVersionRanges($packageName)
{
foreach (self::getInstalled() as $installed) {
if (!isset($installed['versions'][$packageName])) {
continue;
}

$ranges = array();
if (isset($installed['versions'][$packageName]['pretty_version'])) {
$ranges[] = $installed['versions'][$packageName]['pretty_version'];
}
if (array_key_exists('aliases', $installed['versions'][$packageName])) {
$ranges = array_merge($ranges, $installed['versions'][$packageName]['aliases']);
}
if (array_key_exists('replaced', $installed['versions'][$packageName])) {
$ranges = array_merge($ranges, $installed['versions'][$packageName]['replaced']);
}
if (array_key_exists('provided', $installed['versions'][$packageName])) {
$ranges = array_merge($ranges, $installed['versions'][$packageName]['provided']);
}

return implode(' || ', $ranges);
}

throw new \OutOfBoundsException('Package "' . $packageName . '" is not installed');
}





public static function getVersion($packageName)
{
foreach (self::getInstalled() as $installed) {
if (!isset($installed['versions'][$packageName])) {
continue;
}

if (!isset($installed['versions'][$packageName]['version'])) {
return null;
}

return $installed['versions'][$packageName]['version'];
}

throw new \OutOfBoundsException('Package "' . $packageName . '" is not installed');
}





public static function getPrettyVersion($packageName)
{
foreach (self::getInstalled() as $installed) {
if (!isset($installed['versions'][$packageName])) {
continue;
}

if (!isset($installed['versions'][$packageName]['pretty_version'])) {
return null;
}

return $installed['versions'][$packageName]['pretty_version'];
}

throw new \OutOfBoundsException('Package "' . $packageName . '" is not installed');
}





public static function getReference($packageName)
{
foreach (self::getInstalled() as $installed) {
if (!isset($installed['versions'][$packageName])) {
continue;
}

if (!isset($installed['versions'][$packageName]['reference'])) {
return null;
}

return $installed['versions'][$packageName]['reference'];
}

throw new \OutOfBoundsException('Package "' . $packageName . '" is not installed');
}





public static function getRootPackage()
{
$installed = self::getInstalled();

return $installed[0]['root'];
}







public static function getRawData()
{
return self::$installed;
}



















public static function reload($data)
{
self::$installed = $data;
self::$installedByVendor = array();
}




private static function getInstalled()
{
if (null === self::$canGetVendors) {
self::$canGetVendors = method_exists('Composer\Autoload\ClassLoader', 'getRegisteredLoaders');
}

$installed = array();

if (self::$canGetVendors) {

foreach (ClassLoader::getRegisteredLoaders() as $vendorDir => $loader) {
if (isset(self::$installedByVendor[$vendorDir])) {
$installed[] = self::$installedByVendor[$vendorDir];
} elseif (is_file($vendorDir.'/composer/installed.php')) {
$installed[] = self::$installedByVendor[$vendorDir] = require $vendorDir.'/composer/installed.php';
}
}
}

$installed[] = self::$installed;

return $installed;
}
}
