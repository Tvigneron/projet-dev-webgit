<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* evenement/show.html.twig */
class __TwigTemplate_ccf9bd8391e0ba68cc342d0474099692d4907355416a492cea67bd2e9ee63a9e extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "evenement/show.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "evenement/show.html.twig"));

        $this->parent = $this->loadTemplate("base.html.twig", "evenement/show.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 3
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "    <h1>Détails de l'evenement <i>";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["evenement"]) || array_key_exists("evenement", $context) ? $context["evenement"] : (function () { throw new RuntimeError('Variable "evenement" does not exist.', 4, $this->source); })()), "titre", [], "any", false, false, false, 4), "html", null, true);
        echo "</i>:</h1>
    <div class=\"media\" style=\"margin-top: 60px;\">
        <div class=\"media-body\">
            <div class=\"row\">
                <div class=\"col-sm-10\">
                    <h3 class=\"media-heading\"><strong>Lieu :</strong>";
        // line 9
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["evenement"]) || array_key_exists("evenement", $context) ? $context["evenement"] : (function () { throw new RuntimeError('Variable "evenement" does not exist.', 9, $this->source); })()), "adresse", [], "any", false, false, false, 9), "html", null, true);
        echo " <i>(";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["evenement"]) || array_key_exists("evenement", $context) ? $context["evenement"] : (function () { throw new RuntimeError('Variable "evenement" does not exist.', 9, $this->source); })()), "ville", [], "any", false, false, false, 9), "html", null, true);
        echo ")</i></h3>
                    <h3 class=\"media-heading\"><strong>Date de l'evenement : </strong>";
        // line 10
        echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["evenement"]) || array_key_exists("evenement", $context) ? $context["evenement"] : (function () { throw new RuntimeError('Variable "evenement" does not exist.', 10, $this->source); })()), "date", [], "any", false, false, false, 10), "d/m/Y"), "html", null, true);
        echo "</h3>
                </div>
            </div>
            <div>
                <h3> <strong>Description</strong></h3>
                <p>";
        // line 15
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["evenement"]) || array_key_exists("evenement", $context) ? $context["evenement"] : (function () { throw new RuntimeError('Variable "evenement" does not exist.', 15, $this->source); })()), "description", [], "any", false, false, false, 15), "html", null, true);
        echo "</p>
            </div>
            <div class=\"row\" style=\"margin-bottom: 60px;\">
                <div class=\"col-sm-12 text-left\">
                    ";
        // line 19
        if (twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 19, $this->source); })()), "user", [], "any", false, false, false, 19)) {
            // line 20
            echo "                    <a class=\"btn btn-default \" href=\"";
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("participe.user", ["id" => twig_get_attribute($this->env, $this->source, (isset($context["evenement"]) || array_key_exists("evenement", $context) ? $context["evenement"] : (function () { throw new RuntimeError('Variable "evenement" does not exist.', 20, $this->source); })()), "id", [], "any", false, false, false, 20)]), "html", null, true);
            echo "\">
                        <span class=\"glyphicon glyphicon-ok\" aria-hidden=\"true\"></span>";
            // line 21
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("participer"), "html", null, true);
            echo "
                        
                    </a> 
                    ";
        }
        // line 25
        echo "                    <a class=\"btn btn-default \" href=\"";
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("evenement.list");
        echo "\">
                        <span class=\"glyphicon glyphicon-menu-left\" aria-hidden=\"true\"></span>";
        // line 26
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Liste des evenements"), "html", null, true);
        echo "
                        
                    </a>
                    ";
        // line 29
        if ($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("ROLE_ADMIN")) {
            // line 30
            echo "                    <a href=\"";
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("evenement.edit", ["id" => twig_get_attribute($this->env, $this->source, (isset($context["evenement"]) || array_key_exists("evenement", $context) ? $context["evenement"] : (function () { throw new RuntimeError('Variable "evenement" does not exist.', 30, $this->source); })()), "id", [], "any", false, false, false, 30)]), "html", null, true);
            echo "\">
                        <button type=\"button\" class=\"btn btn-warning glyphicon glyphicon-pencil\">";
            // line 31
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Editer"), "html", null, true);
            echo "</button>
                    </a>
                    ";
        }
        // line 34
        echo "                    
                </div>
            </div>
        </div>
        ";
        // line 38
        if ( !twig_test_empty((isset($context["commentaires"]) || array_key_exists("commentaires", $context) ? $context["commentaires"] : (function () { throw new RuntimeError('Variable "commentaires" does not exist.', 38, $this->source); })()))) {
            // line 39
            echo "            <table class=\"table text-center\">
                <tr>
                    <th class=\"active text-center\">";
            // line 41
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Utilisateur"), "html", null, true);
            echo " </th>
                    <th class=\"active text-center\">";
            // line 42
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Commentaire"), "html", null, true);
            echo "</th>
                </tr>

                ";
            // line 45
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["commentaires"]) || array_key_exists("commentaires", $context) ? $context["commentaires"] : (function () { throw new RuntimeError('Variable "commentaires" does not exist.', 45, $this->source); })()));
            foreach ($context['_seq'] as $context["_key"] => $context["commentaire"]) {
                // line 46
                echo "                    <tr>
                        <td>";
                // line 47
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["commentaire"], "getUtilisateur", [], "method", false, false, false, 47), "getNom", [], "method", false, false, false, 47), "html", null, true);
                echo "  ";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["commentaire"], "getUtilisateur", [], "method", false, false, false, 47), "getPrenom", [], "method", false, false, false, 47), "html", null, true);
                echo "</td>
                        <td>";
                // line 48
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["commentaire"], "description", [], "any", false, false, false, 48), "html", null, true);
                echo "</td>
                        ";
                // line 49
                if (twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 49, $this->source); })()), "user", [], "any", false, false, false, 49)) {
                    // line 50
                    echo "                            ";
                    if ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["commentaire"], "getUtilisateur", [], "method", false, false, false, 50), "getId", [], "method", false, false, false, 50) == twig_get_attribute($this->env, $this->source, (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new RuntimeError('Variable "user" does not exist.', 50, $this->source); })()), "getId", [], "method", false, false, false, 50))) {
                        // line 51
                        echo "                            <td>
                                <a href=\"";
                        // line 52
                        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("commentaire.delete", ["id" => twig_get_attribute($this->env, $this->source, $context["commentaire"], "id", [], "any", false, false, false, 52)]), "html", null, true);
                        echo "\">
                                    <button type=\"button\" class=\"btn btn-warning\">";
                        // line 53
                        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(" Supprimer"), "html", null, true);
                        echo "</button>
                                </a>
                            </td>
                            ";
                    }
                    // line 57
                    echo "                        ";
                }
                // line 58
                echo "                    </tr>
                    
                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['commentaire'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 61
            echo "            </table>
        ";
        } else {
            // line 63
            echo "            <table class=\"table text-center\">
                <tr>
                        <th class=\"active text-center\">";
            // line 65
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Aucun commentaire"), "html", null, true);
            echo "</th>
                </tr>
            </table>
                
        ";
        }
        // line 70
        echo "        ";
        if (twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 70, $this->source); })()), "user", [], "any", false, false, false, 70)) {
            // line 71
            echo "            <div>
                ";
            // line 72
            echo             $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 72, $this->source); })()), 'form_start');
            echo "
                ";
            // line 73
            echo             $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 73, $this->source); })()), 'form_end');
            echo "
            </div>
        ";
        }
        // line 76
        echo "        
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "evenement/show.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  237 => 76,  231 => 73,  227 => 72,  224 => 71,  221 => 70,  213 => 65,  209 => 63,  205 => 61,  197 => 58,  194 => 57,  187 => 53,  183 => 52,  180 => 51,  177 => 50,  175 => 49,  171 => 48,  165 => 47,  162 => 46,  158 => 45,  152 => 42,  148 => 41,  144 => 39,  142 => 38,  136 => 34,  130 => 31,  125 => 30,  123 => 29,  117 => 26,  112 => 25,  105 => 21,  100 => 20,  98 => 19,  91 => 15,  83 => 10,  77 => 9,  68 => 4,  58 => 3,  35 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends 'base.html.twig' %}

{% block body %}
    <h1>Détails de l'evenement <i>{{ evenement.titre }}</i>:</h1>
    <div class=\"media\" style=\"margin-top: 60px;\">
        <div class=\"media-body\">
            <div class=\"row\">
                <div class=\"col-sm-10\">
                    <h3 class=\"media-heading\"><strong>Lieu :</strong>{{ evenement.adresse }} <i>({{ evenement.ville }})</i></h3>
                    <h3 class=\"media-heading\"><strong>Date de l'evenement : </strong>{{  evenement.date|date(\"d/m/Y\") }}</h3>
                </div>
            </div>
            <div>
                <h3> <strong>Description</strong></h3>
                <p>{{ evenement.description }}</p>
            </div>
            <div class=\"row\" style=\"margin-bottom: 60px;\">
                <div class=\"col-sm-12 text-left\">
                    {% if app.user %}
                    <a class=\"btn btn-default \" href=\"{{ path('participe.user', {'id' :evenement.id} ) }}\">
                        <span class=\"glyphicon glyphicon-ok\" aria-hidden=\"true\"></span>{{'participer'|trans}}
                        
                    </a> 
                    {% endif %}
                    <a class=\"btn btn-default \" href=\"{{ path('evenement.list' ) }}\">
                        <span class=\"glyphicon glyphicon-menu-left\" aria-hidden=\"true\"></span>{{'Liste des evenements'|trans}}
                        
                    </a>
                    {% if is_granted('ROLE_ADMIN') %}
                    <a href=\"{{ path('evenement.edit', {id: evenement.id} ) }}\">
                        <button type=\"button\" class=\"btn btn-warning glyphicon glyphicon-pencil\">{{'Editer'|trans}}</button>
                    </a>
                    {% endif %}
                    
                </div>
            </div>
        </div>
        {% if commentaires is not empty %}
            <table class=\"table text-center\">
                <tr>
                    <th class=\"active text-center\">{{'Utilisateur'|trans}} </th>
                    <th class=\"active text-center\">{{'Commentaire'|trans}}</th>
                </tr>

                {% for commentaire in commentaires %}
                    <tr>
                        <td>{{ commentaire.getUtilisateur().getNom() }}  {{ commentaire.getUtilisateur().getPrenom() }}</td>
                        <td>{{ commentaire.description }}</td>
                        {% if app.user %}
                            {% if commentaire.getUtilisateur().getId() == user.getId()  %}
                            <td>
                                <a href=\"{{ path('commentaire.delete', {id: commentaire.id} ) }}\">
                                    <button type=\"button\" class=\"btn btn-warning\">{{' Supprimer'|trans}}</button>
                                </a>
                            </td>
                            {% endif %}
                        {% endif %}
                    </tr>
                    
                {% endfor %}
            </table>
        {% else %}
            <table class=\"table text-center\">
                <tr>
                        <th class=\"active text-center\">{{'Aucun commentaire'|trans }}</th>
                </tr>
            </table>
                
        {% endif %}
        {% if app.user %}
            <div>
                {{ form_start(form) }}
                {{ form_end(form) }}
            </div>
        {% endif %}
        
{% endblock %}", "evenement/show.html.twig", "C:\\xampp\\htdocs\\Projet\\projet\\templates\\evenement\\show.html.twig");
    }
}
