<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* evenement/list.html.twig */
class __TwigTemplate_d211644f5889e328c990cb251153b8a18db86c4e1d86eca640370f7fe51b115a extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "evenement/list.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "evenement/list.html.twig"));

        $this->parent = $this->loadTemplate("base.html.twig", "evenement/list.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 3
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "    <h5><B>";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Ci-dessous vous allez trouver tous les événements disponibles autour des jeux vidéos. Afin de participer aux événements, vous devez vous connecter !"), "html", null, true);
        echo "</B> </h5>
    <table class=\"table text-center\">
        <tr>
            <th class=\"active text-center\">";
        // line 7
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Titre"), "html", null, true);
        echo "</th>
            <th class=\"active text-center\">";
        // line 8
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Date"), "html", null, true);
        echo "</th>
            <th class=\"active text-center\">";
        // line 9
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Adresse"), "html", null, true);
        echo "</th>
            <th class=\"active text-center\">";
        // line 10
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Ville"), "html", null, true);
        echo "</th>
            <th class=\"active text-center\">";
        // line 11
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Jeux"), "html", null, true);
        echo "</th>
            <th class=\"active text-center\">";
        // line 12
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Description"), "html", null, true);
        echo "</th>
            ";
        // line 13
        if ($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("ROLE_ADMIN")) {
            // line 14
            echo "                <th class=\"active text-center\">";
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Editer"), "html", null, true);
            echo "</th>
                <th class=\"active text-center\">";
            // line 15
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Supression"), "html", null, true);
            echo "</th>
            ";
        }
        // line 17
        echo "            

        </tr>

        ";
        // line 21
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["evenements"]) || array_key_exists("evenements", $context) ? $context["evenements"] : (function () { throw new RuntimeError('Variable "evenements" does not exist.', 21, $this->source); })()));
        foreach ($context['_seq'] as $context["_key"] => $context["evenement"]) {
            // line 22
            echo "            <tr>
                <td><a href=\"";
            // line 23
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("evenement.show", ["id" => twig_get_attribute($this->env, $this->source, $context["evenement"], "id", [], "any", false, false, false, 23)]), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["evenement"], "titre", [], "any", false, false, false, 23), "html", null, true);
            echo "</a></td>
                <td> ";
            // line 24
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, $context["evenement"], "date", [], "any", false, false, false, 24), "d/m/Y"), "html", null, true);
            echo "  </td>
                <td>";
            // line 25
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["evenement"], "adresse", [], "any", false, false, false, 25), "html", null, true);
            echo "</td>
                <td>";
            // line 26
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["evenement"], "ville", [], "any", false, false, false, 26), "html", null, true);
            echo "</td>
                <td>";
            // line 27
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["evenement"], "jeux", [], "any", false, false, false, 27), "html", null, true);
            echo "</td>
                <td class=\"overflow-ellipsis\">";
            // line 28
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["evenement"], "description", [], "any", false, false, false, 28), "html", null, true);
            echo "</td>
                ";
            // line 29
            if ($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("ROLE_ADMIN")) {
                // line 30
                echo "                   <td>
                    <form class=\"navbar-form navbar-center\">
                        <a href=\"";
                // line 32
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("evenement.edit", ["id" => twig_get_attribute($this->env, $this->source, $context["evenement"], "id", [], "any", false, false, false, 32)]), "html", null, true);
                echo "\">
                            <button type=\"button\" class=\"btn btn-warning\">";
                // line 33
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Editer"), "html", null, true);
                echo "</button>
                        </a>
                    </form>
                </td>
                <td>
                    <form class=\"navbar-form navbar-center\">
                        <a href=\"";
                // line 39
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("evenement.delete", ["id" => twig_get_attribute($this->env, $this->source, $context["evenement"], "id", [], "any", false, false, false, 39)]), "html", null, true);
                echo "\">
                            <button type=\"button\" class=\"btn btn-danger\">";
                // line 40
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Suprimer"), "html", null, true);
                echo "</button>
                        </a>
                    </form>
                </td> 
                ";
            }
            // line 45
            echo "                

            </tr>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['evenement'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 49
        echo "    </table>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "evenement/list.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  190 => 49,  181 => 45,  173 => 40,  169 => 39,  160 => 33,  156 => 32,  152 => 30,  150 => 29,  146 => 28,  142 => 27,  138 => 26,  134 => 25,  130 => 24,  124 => 23,  121 => 22,  117 => 21,  111 => 17,  106 => 15,  101 => 14,  99 => 13,  95 => 12,  91 => 11,  87 => 10,  83 => 9,  79 => 8,  75 => 7,  68 => 4,  58 => 3,  35 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends 'base.html.twig' %}

{% block body %}
    <h5><B>{{'Ci-dessous vous allez trouver tous les événements disponibles autour des jeux vidéos. Afin de participer aux événements, vous devez vous connecter !'|trans}}</B> </h5>
    <table class=\"table text-center\">
        <tr>
            <th class=\"active text-center\">{{'Titre'|trans }}</th>
            <th class=\"active text-center\">{{'Date'|trans }}</th>
            <th class=\"active text-center\">{{'Adresse'|trans }}</th>
            <th class=\"active text-center\">{{'Ville'|trans }}</th>
            <th class=\"active text-center\">{{'Jeux'|trans }}</th>
            <th class=\"active text-center\">{{'Description'|trans }}</th>
            {% if is_granted('ROLE_ADMIN') %}
                <th class=\"active text-center\">{{'Editer'|trans }}</th>
                <th class=\"active text-center\">{{'Supression'|trans }}</th>
            {% endif %}
            

        </tr>

        {% for evenement in evenements %}
            <tr>
                <td><a href=\"{{ path('evenement.show', {id: evenement.id}) }}\">{{ evenement.titre }}</a></td>
                <td> {{ evenement.date|date(\"d/m/Y\") }}  </td>
                <td>{{ evenement.adresse }}</td>
                <td>{{ evenement.ville }}</td>
                <td>{{ evenement.jeux }}</td>
                <td class=\"overflow-ellipsis\">{{ evenement.description }}</td>
                {% if is_granted('ROLE_ADMIN') %}
                   <td>
                    <form class=\"navbar-form navbar-center\">
                        <a href=\"{{ path('evenement.edit', {id: evenement.id} ) }}\">
                            <button type=\"button\" class=\"btn btn-warning\">{{'Editer'|trans }}</button>
                        </a>
                    </form>
                </td>
                <td>
                    <form class=\"navbar-form navbar-center\">
                        <a href=\"{{ path('evenement.delete', {id: evenement.id} ) }}\">
                            <button type=\"button\" class=\"btn btn-danger\">{{'Suprimer'|trans }}</button>
                        </a>
                    </form>
                </td> 
                {% endif %}
                

            </tr>
        {% endfor %}
    </table>
{% endblock %}", "evenement/list.html.twig", "C:\\xampp\\htdocs\\Projet\\projet\\templates\\evenement\\list.html.twig");
    }
}
