<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* user/show.html.twig */
class __TwigTemplate_352e934a40d55b0561b6e401d599b942f5a0fac26c5d4420abef562f408ba36b extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "user/show.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "user/show.html.twig"));

        $this->parent = $this->loadTemplate("base.html.twig", "user/show.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 3
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "    <h1><strong>";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Mon compte"), "html", null, true);
        echo "</strong></h1>
    <div class=\"media\" style=\"margin-top: 20px;\">
        <div class=\"media-body\">
            <div class=\"row\">
                <div class=\"col-sm-10\">
                    <h3 class=\"media-heading\">";
        // line 9
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Nom :"), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new RuntimeError('Variable "user" does not exist.', 9, $this->source); })()), "nom", [], "any", false, false, false, 9), "html", null, true);
        echo " </h3>
                    <h3 class=\"media-heading\">";
        // line 10
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Prenom :"), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new RuntimeError('Variable "user" does not exist.', 10, $this->source); })()), "prenom", [], "any", false, false, false, 10), "html", null, true);
        echo "</h3>
                    <h3 class=\"media-heading\">";
        // line 11
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Age :"), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new RuntimeError('Variable "user" does not exist.', 11, $this->source); })()), "age", [], "any", false, false, false, 11), "html", null, true);
        echo "</h3>
                    <h3 class=\"media-heading\">";
        // line 12
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("AdresseMail :"), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new RuntimeError('Variable "user" does not exist.', 12, $this->source); })()), "email", [], "any", false, false, false, 12), "html", null, true);
        echo "</h3>
                    <h3 class=\"media-heading\">";
        // line 13
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Numero :"), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new RuntimeError('Variable "user" does not exist.', 13, $this->source); })()), "numero", [], "any", false, false, false, 13), "html", null, true);
        echo "</h3>
                </div>
            </div >
        </div>
    </div>
    
    <h2>";
        // line 19
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Mes evenements"), "html", null, true);
        echo "</h2>
    ";
        // line 20
        if ( !twig_test_empty((isset($context["evenements"]) || array_key_exists("evenements", $context) ? $context["evenements"] : (function () { throw new RuntimeError('Variable "evenements" does not exist.', 20, $this->source); })()))) {
            // line 21
            echo "    <table class=\"table text-center\">
        <tr>
            <th class=\"active text-center\">";
            // line 23
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Titre"), "html", null, true);
            echo "</th>
            <th class=\"active text-center\">";
            // line 24
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Date"), "html", null, true);
            echo "</th>
            <th class=\"active text-center\">";
            // line 25
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Adresse"), "html", null, true);
            echo "</th>
            <th class=\"active text-center\">";
            // line 26
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Ville"), "html", null, true);
            echo "</th>
            <th class=\"active text-center\">";
            // line 27
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Jeux"), "html", null, true);
            echo "</th>
            <th class=\"active text-center\">";
            // line 28
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Description"), "html", null, true);
            echo "</th>
            <th class=\"active text-center\">";
            // line 29
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Desinsciption"), "html", null, true);
            echo "</th>
        </tr>
        ";
            // line 31
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["evenements"]) || array_key_exists("evenements", $context) ? $context["evenements"] : (function () { throw new RuntimeError('Variable "evenements" does not exist.', 31, $this->source); })()));
            foreach ($context['_seq'] as $context["_key"] => $context["evenement"]) {
                // line 32
                echo "            <tr>
                <td><a href=\"";
                // line 33
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("evenement.show", ["id" => twig_get_attribute($this->env, $this->source, $context["evenement"], "id", [], "any", false, false, false, 33)]), "html", null, true);
                echo "\">";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["evenement"], "titre", [], "any", false, false, false, 33), "html", null, true);
                echo "</a></td>
                <td>  ";
                // line 34
                echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, $context["evenement"], "date", [], "any", false, false, false, 34), "d/m/Y"), "html", null, true);
                echo "  </td>
                <td>";
                // line 35
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["evenement"], "adresse", [], "any", false, false, false, 35), "html", null, true);
                echo "</td>
                <td>";
                // line 36
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["evenement"], "ville", [], "any", false, false, false, 36), "html", null, true);
                echo "</td>
                <td>";
                // line 37
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["evenement"], "jeux", [], "any", false, false, false, 37), "html", null, true);
                echo "</td>
                <td>";
                // line 38
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["evenement"], "description", [], "any", false, false, false, 38), "html", null, true);
                echo "</td>
                <td>
                    <form class=\"center\">
                        <a href=\"";
                // line 41
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("user.sedesinscrire", ["id" => twig_get_attribute($this->env, $this->source, $context["evenement"], "id", [], "any", false, false, false, 41)]), "html", null, true);
                echo "\">
                            <button type=\"button\" class=\"btn btn-warning\">";
                // line 42
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Se desinscrire"), "html", null, true);
                echo "</button>
                        </a>
                    </form>
                </td>
            </tr>
        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['evenement'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 48
            echo "    </table>
    ";
        } else {
            // line 50
            echo "        <table class=\"table text-center\">
            <tr>
                <th class=\"active text-center\">";
            // line 52
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Aucun evenement"), "html", null, true);
            echo " </th>
            </tr>
        </table>
    ";
        }
        // line 56
        echo "        <h2>";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Mes Commentaires"), "html", null, true);
        echo "</h2>
     ";
        // line 57
        if ( !twig_test_empty((isset($context["commentaires"]) || array_key_exists("commentaires", $context) ? $context["commentaires"] : (function () { throw new RuntimeError('Variable "commentaires" does not exist.', 57, $this->source); })()))) {
            // line 58
            echo "            <table class=\"table text-center\">
                <tr>
                    <th class=\"active text-center\">";
            // line 60
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Utilisateur"), "html", null, true);
            echo " </th>
                    <th class=\"active text-center\">";
            // line 61
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Commentaire"), "html", null, true);
            echo "</th>
                    <th class=\"active text-center\">";
            // line 62
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Supression"), "html", null, true);
            echo "</th>
                </tr>

                ";
            // line 65
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["commentaires"]) || array_key_exists("commentaires", $context) ? $context["commentaires"] : (function () { throw new RuntimeError('Variable "commentaires" does not exist.', 65, $this->source); })()));
            foreach ($context['_seq'] as $context["_key"] => $context["commentaire"]) {
                // line 66
                echo "                    <tr>
                        <td>";
                // line 67
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["commentaire"], "getUtilisateur", [], "method", false, false, false, 67), "getNom", [], "method", false, false, false, 67), "html", null, true);
                echo "  ";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["commentaire"], "getUtilisateur", [], "method", false, false, false, 67), "getPrenom", [], "method", false, false, false, 67), "html", null, true);
                echo "</td>
                        <td>";
                // line 68
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["commentaire"], "description", [], "any", false, false, false, 68), "html", null, true);
                echo "</td>
                        <td>
                            <a href=\"";
                // line 70
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("commentaire.delete", ["id" => twig_get_attribute($this->env, $this->source, $context["commentaire"], "id", [], "any", false, false, false, 70)]), "html", null, true);
                echo "\">
                                <button type=\"button\" class=\"btn btn-warning\">";
                // line 71
                echo " Supprimer";
                echo "</button>
                            </a>
                        </td>
                    </tr>
                    
                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['commentaire'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 77
            echo "            </table>
        ";
        } else {
            // line 79
            echo "            <table class=\"table text-center\">
                <tr>
                        <th class=\"active text-center\">";
            // line 81
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Aucun commentaire"), "html", null, true);
            echo " </th>
                </tr>
            </table>
                
        ";
        }
        // line 86
        echo "    </table>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "user/show.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  292 => 86,  284 => 81,  280 => 79,  276 => 77,  264 => 71,  260 => 70,  255 => 68,  249 => 67,  246 => 66,  242 => 65,  236 => 62,  232 => 61,  228 => 60,  224 => 58,  222 => 57,  217 => 56,  210 => 52,  206 => 50,  202 => 48,  190 => 42,  186 => 41,  180 => 38,  176 => 37,  172 => 36,  168 => 35,  164 => 34,  158 => 33,  155 => 32,  151 => 31,  146 => 29,  142 => 28,  138 => 27,  134 => 26,  130 => 25,  126 => 24,  122 => 23,  118 => 21,  116 => 20,  112 => 19,  101 => 13,  95 => 12,  89 => 11,  83 => 10,  77 => 9,  68 => 4,  58 => 3,  35 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends 'base.html.twig' %}

{% block body %}
    <h1><strong>{{'Mon compte'|trans}}</strong></h1>
    <div class=\"media\" style=\"margin-top: 20px;\">
        <div class=\"media-body\">
            <div class=\"row\">
                <div class=\"col-sm-10\">
                    <h3 class=\"media-heading\">{{'Nom :'|trans }} {{ user.nom  }} </h3>
                    <h3 class=\"media-heading\">{{'Prenom :'|trans }} {{ user.prenom }}</h3>
                    <h3 class=\"media-heading\">{{'Age :'|trans }} {{ user.age }}</h3>
                    <h3 class=\"media-heading\">{{'AdresseMail :'|trans }} {{ user.email }}</h3>
                    <h3 class=\"media-heading\">{{'Numero :'|trans }} {{ user.numero }}</h3>
                </div>
            </div >
        </div>
    </div>
    
    <h2>{{'Mes evenements'|trans }}</h2>
    {% if evenements is not empty %}
    <table class=\"table text-center\">
        <tr>
            <th class=\"active text-center\">{{'Titre'|trans }}</th>
            <th class=\"active text-center\">{{'Date'|trans }}</th>
            <th class=\"active text-center\">{{'Adresse'|trans }}</th>
            <th class=\"active text-center\">{{'Ville'|trans }}</th>
            <th class=\"active text-center\">{{'Jeux'|trans }}</th>
            <th class=\"active text-center\">{{'Description'|trans }}</th>
            <th class=\"active text-center\">{{'Desinsciption'|trans }}</th>
        </tr>
        {% for evenement in evenements %}
            <tr>
                <td><a href=\"{{ path('evenement.show', {id: evenement.id}) }}\">{{ evenement.titre }}</a></td>
                <td>  {{ evenement.date|date(\"d/m/Y\") }}  </td>
                <td>{{ evenement.adresse }}</td>
                <td>{{ evenement.ville }}</td>
                <td>{{ evenement.jeux }}</td>
                <td>{{ evenement.description }}</td>
                <td>
                    <form class=\"center\">
                        <a href=\"{{ path('user.sedesinscrire', {id: evenement.id} ) }}\">
                            <button type=\"button\" class=\"btn btn-warning\">{{'Se desinscrire'|trans }}</button>
                        </a>
                    </form>
                </td>
            </tr>
        {% endfor %}
    </table>
    {% else %}
        <table class=\"table text-center\">
            <tr>
                <th class=\"active text-center\">{{'Aucun evenement'|trans }} </th>
            </tr>
        </table>
    {% endif %}
        <h2>{{'Mes Commentaires'|trans }}</h2>
     {% if commentaires is not empty %}
            <table class=\"table text-center\">
                <tr>
                    <th class=\"active text-center\">{{'Utilisateur'|trans }} </th>
                    <th class=\"active text-center\">{{'Commentaire'|trans }}</th>
                    <th class=\"active text-center\">{{'Supression'|trans }}</th>
                </tr>

                {% for commentaire in commentaires %}
                    <tr>
                        <td>{{ commentaire.getUtilisateur().getNom() }}  {{ commentaire.getUtilisateur().getPrenom() }}</td>
                        <td>{{ commentaire.description }}</td>
                        <td>
                            <a href=\"{{ path('commentaire.delete', {id: commentaire.id} ) }}\">
                                <button type=\"button\" class=\"btn btn-warning\">{{' Supprimer'}}</button>
                            </a>
                        </td>
                    </tr>
                    
                {% endfor %}
            </table>
        {% else %}
            <table class=\"table text-center\">
                <tr>
                        <th class=\"active text-center\">{{'Aucun commentaire'|trans }} </th>
                </tr>
            </table>
                
        {% endif %}
    </table>
{% endblock %}", "user/show.html.twig", "C:\\xampp\\htdocs\\Projet\\projet\\templates\\user\\show.html.twig");
    }
}
