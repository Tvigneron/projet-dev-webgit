<?php

use Symfony\Component\DependencyInjection\Argument\RewindableGenerator;
use Symfony\Component\DependencyInjection\Exception\RuntimeException;

// This file has been auto-generated by the Symfony Dependency Injection Component for internal use.
// Returns the private 'debug.argument_resolver.service' shared service.

include_once $this->targetDirs[3].'\\vendor\\symfony\\http-kernel\\Controller\\ArgumentValueResolverInterface.php';
include_once $this->targetDirs[3].'\\vendor\\symfony\\http-kernel\\Controller\\ArgumentResolver\\TraceableValueResolver.php';
include_once $this->targetDirs[3].'\\vendor\\symfony\\http-kernel\\Controller\\ArgumentResolver\\ServiceValueResolver.php';

return $this->privates['debug.argument_resolver.service'] = new \Symfony\Component\HttpKernel\Controller\ArgumentResolver\TraceableValueResolver(new \Symfony\Component\HttpKernel\Controller\ArgumentResolver\ServiceValueResolver(new \Symfony\Component\DependencyInjection\Argument\ServiceLocator($this->getService, [
    'App\\Controller\\AccessDeniedController::handle' => ['privates', '.service_locator.ctrS4ZE', 'get_ServiceLocator_CtrS4ZEService.php', true],
    'App\\Controller\\CommentaireController::delete' => ['privates', '.service_locator.pe89cNF', 'get_ServiceLocator_Pe89cNFService.php', true],
    'App\\Controller\\EvenementController::create' => ['privates', '.service_locator.pe89cNF', 'get_ServiceLocator_Pe89cNFService.php', true],
    'App\\Controller\\EvenementController::delete' => ['privates', '.service_locator.LavTAzQ', 'get_ServiceLocator_LavTAzQService.php', true],
    'App\\Controller\\EvenementController::edit' => ['privates', '.service_locator.LavTAzQ', 'get_ServiceLocator_LavTAzQService.php', true],
    'App\\Controller\\EvenementController::show' => ['privates', '.service_locator.LavTAzQ', 'get_ServiceLocator_LavTAzQService.php', true],
    'App\\Controller\\SearchController::searchEvenement' => ['privates', '.service_locator.E2hqRc_', 'get_ServiceLocator_E2hqRcService.php', true],
    'App\\Controller\\SecurityController::login' => ['privates', '.service_locator.EbLunuf', 'get_ServiceLocator_EbLunufService.php', true],
    'App\\Controller\\UserController::creation' => ['privates', '.service_locator.Ow9IBJM', 'get_ServiceLocator_Ow9IBJMService.php', true],
    'App\\Controller\\UserController::participe' => ['privates', '.service_locator.pe89cNF', 'get_ServiceLocator_Pe89cNFService.php', true],
    'App\\Controller\\UserController::sedesinscrire' => ['privates', '.service_locator.pe89cNF', 'get_ServiceLocator_Pe89cNFService.php', true],
    'App\\Controller\\AccessDeniedController:handle' => ['privates', '.service_locator.ctrS4ZE', 'get_ServiceLocator_CtrS4ZEService.php', true],
    'App\\Controller\\CommentaireController:delete' => ['privates', '.service_locator.pe89cNF', 'get_ServiceLocator_Pe89cNFService.php', true],
    'App\\Controller\\EvenementController:create' => ['privates', '.service_locator.pe89cNF', 'get_ServiceLocator_Pe89cNFService.php', true],
    'App\\Controller\\EvenementController:delete' => ['privates', '.service_locator.LavTAzQ', 'get_ServiceLocator_LavTAzQService.php', true],
    'App\\Controller\\EvenementController:edit' => ['privates', '.service_locator.LavTAzQ', 'get_ServiceLocator_LavTAzQService.php', true],
    'App\\Controller\\EvenementController:show' => ['privates', '.service_locator.LavTAzQ', 'get_ServiceLocator_LavTAzQService.php', true],
    'App\\Controller\\SearchController:searchEvenement' => ['privates', '.service_locator.E2hqRc_', 'get_ServiceLocator_E2hqRcService.php', true],
    'App\\Controller\\SecurityController:login' => ['privates', '.service_locator.EbLunuf', 'get_ServiceLocator_EbLunufService.php', true],
    'App\\Controller\\UserController:creation' => ['privates', '.service_locator.Ow9IBJM', 'get_ServiceLocator_Ow9IBJMService.php', true],
    'App\\Controller\\UserController:participe' => ['privates', '.service_locator.pe89cNF', 'get_ServiceLocator_Pe89cNFService.php', true],
    'App\\Controller\\UserController:sedesinscrire' => ['privates', '.service_locator.pe89cNF', 'get_ServiceLocator_Pe89cNFService.php', true],
])), ($this->privates['debug.stopwatch'] ?? ($this->privates['debug.stopwatch'] = new \Symfony\Component\Stopwatch\Stopwatch(true))));
