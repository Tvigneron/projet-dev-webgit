<?php

use Symfony\Component\Routing\Matcher\Dumper\PhpMatcherTrait;
use Symfony\Component\Routing\RequestContext;

/**
 * This class has been auto-generated
 * by the Symfony Routing Component.
 */
class srcApp_KernelDevDebugContainerUrlMatcher extends Symfony\Bundle\FrameworkBundle\Routing\RedirectableUrlMatcher
{
    use PhpMatcherTrait;

    public function __construct(RequestContext $context)
    {
        $this->context = $context;
        $this->staticRoutes = [
            '/_profiler' => [[['_route' => '_profiler_home', '_controller' => 'web_profiler.controller.profiler::homeAction'], null, null, null, true, false, null]],
            '/_profiler/search' => [[['_route' => '_profiler_search', '_controller' => 'web_profiler.controller.profiler::searchAction'], null, null, null, false, false, null]],
            '/_profiler/search_bar' => [[['_route' => '_profiler_search_bar', '_controller' => 'web_profiler.controller.profiler::searchBarAction'], null, null, null, false, false, null]],
            '/_profiler/phpinfo' => [[['_route' => '_profiler_phpinfo', '_controller' => 'web_profiler.controller.profiler::phpinfoAction'], null, null, null, false, false, null]],
            '/_profiler/open' => [[['_route' => '_profiler_open_file', '_controller' => 'web_profiler.controller.profiler::openAction'], null, null, null, false, false, null]],
            '/' => [[['_route' => 'index', '_controller' => 'App\\Controller\\EvenementController::list'], null, null, null, false, false, null]],
        ];
        $this->regexpList = [
            0 => '{^(?'
                    .'|/_(?'
                        .'|error/(\\d+)(?:\\.([^/]++))?(*:38)'
                        .'|wdt/([^/]++)(*:57)'
                        .'|profiler/([^/]++)(?'
                            .'|/(?'
                                .'|search/results(*:102)'
                                .'|router(*:116)'
                                .'|exception(?'
                                    .'|(*:136)'
                                    .'|\\.css(*:149)'
                                .')'
                            .')'
                            .'|(*:159)'
                        .')'
                    .')'
                    .'|/([^/]++)(?'
                        .'|/(?'
                            .'|de(?'
                                .'|letecomm/(\\d+)(*:204)'
                                .'|sincrire/(\\d+)(*:226)'
                            .')'
                            .'|evenement/(?'
                                .'|(\\d+)(*:253)'
                                .'|(\\d+)/edit(*:271)'
                                .'|(\\d+)/delete(*:291)'
                            .')'
                            .'|nouvel\\-evenement(*:317)'
                            .'|search(*:331)'
                            .'|log(?'
                                .'|in(*:347)'
                                .'|out(*:358)'
                            .')'
                            .'|c(?'
                                .'|ommanderefuse(*:384)'
                                .'|reercompte(*:402)'
                            .')'
                            .'|participe/(\\d+)(*:426)'
                            .'|user(*:438)'
                        .')'
                        .'|(*:447)'
                    .')'
                    .'|/admin(*:462)'
                .')/?$}sDu',
        ];
        $this->dynamicRoutes = [
            38 => [[['_route' => '_twig_error_test', '_controller' => 'twig.controller.preview_error::previewErrorPageAction', '_format' => 'html'], ['code', '_format'], null, null, false, true, null]],
            57 => [[['_route' => '_wdt', '_controller' => 'web_profiler.controller.profiler::toolbarAction'], ['token'], null, null, false, true, null]],
            102 => [[['_route' => '_profiler_search_results', '_controller' => 'web_profiler.controller.profiler::searchResultsAction'], ['token'], null, null, false, false, null]],
            116 => [[['_route' => '_profiler_router', '_controller' => 'web_profiler.controller.router::panelAction'], ['token'], null, null, false, false, null]],
            136 => [[['_route' => '_profiler_exception', '_controller' => 'web_profiler.controller.exception::showAction'], ['token'], null, null, false, false, null]],
            149 => [[['_route' => '_profiler_exception_css', '_controller' => 'web_profiler.controller.exception::cssAction'], ['token'], null, null, false, false, null]],
            159 => [[['_route' => '_profiler', '_controller' => 'web_profiler.controller.profiler::panelAction'], ['token'], null, null, false, true, null]],
            204 => [[['_route' => 'commentaire.delete', '_controller' => 'App\\Controller\\CommentaireController::delete'], ['_locale', 'id'], null, null, false, true, null]],
            226 => [[['_route' => 'user.sedesinscrire', '_controller' => 'App\\Controller\\UserController::sedesinscrire'], ['_locale', 'id'], null, null, false, true, null]],
            253 => [[['_route' => 'evenement.show', '_controller' => 'App\\Controller\\EvenementController::show'], ['_locale', 'id'], null, null, false, true, null]],
            271 => [[['_route' => 'evenement.edit', '_controller' => 'App\\Controller\\EvenementController::edit'], ['_locale', 'id'], null, null, false, false, null]],
            291 => [[['_route' => 'evenement.delete', '_controller' => 'App\\Controller\\EvenementController::delete'], ['_locale', 'id'], null, null, false, false, null]],
            317 => [[['_route' => 'evenement.create', '_controller' => 'App\\Controller\\EvenementController::create'], ['_locale'], null, null, false, false, null]],
            331 => [[['_route' => 'search', '_controller' => 'App\\Controller\\SearchController::searchEvenement'], ['_locale'], null, null, false, false, null]],
            347 => [[['_route' => 'app_login', '_controller' => 'App\\Controller\\SecurityController::login'], ['_locale'], null, null, false, false, null]],
            358 => [[['_route' => 'app_logout', '_controller' => 'App\\Controller\\SecurityController::logout'], ['_locale'], null, null, false, false, null]],
            384 => [[['_route' => 'app_denied', '_controller' => 'App\\Controller\\SecurityController::fonctionnalite'], ['_locale'], null, null, false, false, null]],
            402 => [[['_route' => 'user.creercompte', '_controller' => 'App\\Controller\\UserController::creation'], ['_locale'], null, null, false, false, null]],
            426 => [[['_route' => 'participe.user', '_controller' => 'App\\Controller\\UserController::participe'], ['_locale', 'id'], null, null, false, true, null]],
            438 => [[['_route' => 'user.show', '_controller' => 'App\\Controller\\UserController::show'], ['_locale'], null, null, false, false, null]],
            447 => [[['_route' => 'evenement.list', '_controller' => 'App\\Controller\\EvenementController::list'], ['_locale'], null, null, true, true, null]],
            462 => [[['_route' => 'easyadmin', '_controller' => 'EasyCorp\\Bundle\\EasyAdminBundle\\Controller\\EasyAdminController::indexAction'], [], null, null, true, false, null]],
        ];
    }
}
