<?php

use Symfony\Component\Routing\RequestContext;
use Symfony\Component\Routing\Exception\RouteNotFoundException;
use Psr\Log\LoggerInterface;

/**
 * This class has been auto-generated
 * by the Symfony Routing Component.
 */
class srcApp_KernelDevDebugContainerUrlGenerator extends Symfony\Component\Routing\Generator\UrlGenerator
{
    private static $declaredRoutes;
    private $defaultLocale;

    public function __construct(RequestContext $context, LoggerInterface $logger = null, string $defaultLocale = null)
    {
        $this->context = $context;
        $this->logger = $logger;
        $this->defaultLocale = $defaultLocale;
        if (null === self::$declaredRoutes) {
            self::$declaredRoutes = [
        '_twig_error_test' => [['code', '_format'], ['_controller' => 'twig.controller.preview_error::previewErrorPageAction', '_format' => 'html'], ['code' => '\\d+'], [['variable', '.', '[^/]++', '_format', true], ['variable', '/', '\\d+', 'code', true], ['text', '/_error']], [], []],
        '_wdt' => [['token'], ['_controller' => 'web_profiler.controller.profiler::toolbarAction'], [], [['variable', '/', '[^/]++', 'token', true], ['text', '/_wdt']], [], []],
        '_profiler_home' => [[], ['_controller' => 'web_profiler.controller.profiler::homeAction'], [], [['text', '/_profiler/']], [], []],
        '_profiler_search' => [[], ['_controller' => 'web_profiler.controller.profiler::searchAction'], [], [['text', '/_profiler/search']], [], []],
        '_profiler_search_bar' => [[], ['_controller' => 'web_profiler.controller.profiler::searchBarAction'], [], [['text', '/_profiler/search_bar']], [], []],
        '_profiler_phpinfo' => [[], ['_controller' => 'web_profiler.controller.profiler::phpinfoAction'], [], [['text', '/_profiler/phpinfo']], [], []],
        '_profiler_search_results' => [['token'], ['_controller' => 'web_profiler.controller.profiler::searchResultsAction'], [], [['text', '/search/results'], ['variable', '/', '[^/]++', 'token', true], ['text', '/_profiler']], [], []],
        '_profiler_open_file' => [[], ['_controller' => 'web_profiler.controller.profiler::openAction'], [], [['text', '/_profiler/open']], [], []],
        '_profiler' => [['token'], ['_controller' => 'web_profiler.controller.profiler::panelAction'], [], [['variable', '/', '[^/]++', 'token', true], ['text', '/_profiler']], [], []],
        '_profiler_router' => [['token'], ['_controller' => 'web_profiler.controller.router::panelAction'], [], [['text', '/router'], ['variable', '/', '[^/]++', 'token', true], ['text', '/_profiler']], [], []],
        '_profiler_exception' => [['token'], ['_controller' => 'web_profiler.controller.exception::showAction'], [], [['text', '/exception'], ['variable', '/', '[^/]++', 'token', true], ['text', '/_profiler']], [], []],
        '_profiler_exception_css' => [['token'], ['_controller' => 'web_profiler.controller.exception::cssAction'], [], [['text', '/exception.css'], ['variable', '/', '[^/]++', 'token', true], ['text', '/_profiler']], [], []],
        'commentaire.delete' => [['_locale', 'id'], ['_controller' => 'App\\Controller\\CommentaireController::delete'], ['id' => '\\d+'], [['variable', '/', '\\d+', 'id', true], ['text', '/deletecomm'], ['variable', '/', '[^/]++', '_locale', true]], [], []],
        'evenement.list' => [['_locale'], ['_controller' => 'App\\Controller\\EvenementController::list'], [], [['text', '/'], ['variable', '/', '[^/]++', '_locale', true]], [], []],
        'evenement.show' => [['_locale', 'id'], ['_controller' => 'App\\Controller\\EvenementController::show'], ['id' => '\\d+'], [['variable', '/', '\\d+', 'id', true], ['text', '/evenement'], ['variable', '/', '[^/]++', '_locale', true]], [], []],
        'evenement.create' => [['_locale'], ['_controller' => 'App\\Controller\\EvenementController::create'], [], [['text', '/nouvel-evenement'], ['variable', '/', '[^/]++', '_locale', true]], [], []],
        'evenement.edit' => [['_locale', 'id'], ['_controller' => 'App\\Controller\\EvenementController::edit'], ['id' => '\\d+'], [['text', '/edit'], ['variable', '/', '\\d+', 'id', true], ['text', '/evenement'], ['variable', '/', '[^/]++', '_locale', true]], [], []],
        'evenement.delete' => [['_locale', 'id'], ['_controller' => 'App\\Controller\\EvenementController::delete'], ['id' => '\\d+'], [['text', '/delete'], ['variable', '/', '\\d+', 'id', true], ['text', '/evenement'], ['variable', '/', '[^/]++', '_locale', true]], [], []],
        'search' => [['_locale'], ['_controller' => 'App\\Controller\\SearchController::searchEvenement'], [], [['text', '/search'], ['variable', '/', '[^/]++', '_locale', true]], [], []],
        'app_login' => [['_locale'], ['_controller' => 'App\\Controller\\SecurityController::login'], [], [['text', '/login'], ['variable', '/', '[^/]++', '_locale', true]], [], []],
        'app_logout' => [['_locale'], ['_controller' => 'App\\Controller\\SecurityController::logout'], [], [['text', '/logout'], ['variable', '/', '[^/]++', '_locale', true]], [], []],
        'app_denied' => [['_locale'], ['_controller' => 'App\\Controller\\SecurityController::fonctionnalite'], [], [['text', '/commanderefuse'], ['variable', '/', '[^/]++', '_locale', true]], [], []],
        'participe.user' => [['_locale', 'id'], ['_controller' => 'App\\Controller\\UserController::participe'], ['id' => '\\d+'], [['variable', '/', '\\d+', 'id', true], ['text', '/participe'], ['variable', '/', '[^/]++', '_locale', true]], [], []],
        'user.show' => [['_locale'], ['_controller' => 'App\\Controller\\UserController::show'], [], [['text', '/user'], ['variable', '/', '[^/]++', '_locale', true]], [], []],
        'user.sedesinscrire' => [['_locale', 'id'], ['_controller' => 'App\\Controller\\UserController::sedesinscrire'], ['id' => '\\d+'], [['variable', '/', '\\d+', 'id', true], ['text', '/desincrire'], ['variable', '/', '[^/]++', '_locale', true]], [], []],
        'user.creercompte' => [['_locale'], ['_controller' => 'App\\Controller\\UserController::creation'], [], [['text', '/creercompte'], ['variable', '/', '[^/]++', '_locale', true]], [], []],
        'easyadmin' => [[], ['_controller' => 'EasyCorp\\Bundle\\EasyAdminBundle\\Controller\\EasyAdminController::indexAction'], [], [['text', '/admin/']], [], []],
        'index' => [[], ['_controller' => 'App\\Controller\\EvenementController::list'], [], [['text', '/']], [], []],
    ];
        }
    }

    public function generate($name, $parameters = [], $referenceType = self::ABSOLUTE_PATH)
    {
        $locale = $parameters['_locale']
            ?? $this->context->getParameter('_locale')
            ?: $this->defaultLocale;

        if (null !== $locale && null !== $name) {
            do {
                if ((self::$declaredRoutes[$name.'.'.$locale][1]['_canonical_route'] ?? null) === $name) {
                    unset($parameters['_locale']);
                    $name .= '.'.$locale;
                    break;
                }
            } while (false !== $locale = strstr($locale, '_', true));
        }

        if (!isset(self::$declaredRoutes[$name])) {
            throw new RouteNotFoundException(sprintf('Unable to generate a URL for the named route "%s" as such route does not exist.', $name));
        }

        list($variables, $defaults, $requirements, $tokens, $hostTokens, $requiredSchemes) = self::$declaredRoutes[$name];

        return $this->doGenerate($variables, $defaults, $requirements, $tokens, $parameters, $name, $referenceType, $hostTokens, $requiredSchemes);
    }
}
