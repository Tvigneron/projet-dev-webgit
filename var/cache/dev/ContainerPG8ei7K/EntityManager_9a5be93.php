<?php

class EntityManager_9a5be93 extends \Doctrine\ORM\EntityManager implements \ProxyManager\Proxy\VirtualProxyInterface
{
    /**
     * @var \Doctrine\ORM\EntityManager|null wrapped object, if the proxy is initialized
     */
    private $valueHoldercad8c = null;

    /**
     * @var \Closure|null initializer responsible for generating the wrapped object
     */
    private $initializerbd045 = null;

    /**
     * @var bool[] map of public properties of the parent class
     */
    private static $publicPropertiese105a = [
        
    ];

    public function getConnection()
    {
        $this->initializerbd045 && ($this->initializerbd045->__invoke($valueHoldercad8c, $this, 'getConnection', array(), $this->initializerbd045) || 1) && $this->valueHoldercad8c = $valueHoldercad8c;

        return $this->valueHoldercad8c->getConnection();
    }

    public function getMetadataFactory()
    {
        $this->initializerbd045 && ($this->initializerbd045->__invoke($valueHoldercad8c, $this, 'getMetadataFactory', array(), $this->initializerbd045) || 1) && $this->valueHoldercad8c = $valueHoldercad8c;

        return $this->valueHoldercad8c->getMetadataFactory();
    }

    public function getExpressionBuilder()
    {
        $this->initializerbd045 && ($this->initializerbd045->__invoke($valueHoldercad8c, $this, 'getExpressionBuilder', array(), $this->initializerbd045) || 1) && $this->valueHoldercad8c = $valueHoldercad8c;

        return $this->valueHoldercad8c->getExpressionBuilder();
    }

    public function beginTransaction()
    {
        $this->initializerbd045 && ($this->initializerbd045->__invoke($valueHoldercad8c, $this, 'beginTransaction', array(), $this->initializerbd045) || 1) && $this->valueHoldercad8c = $valueHoldercad8c;

        return $this->valueHoldercad8c->beginTransaction();
    }

    public function getCache()
    {
        $this->initializerbd045 && ($this->initializerbd045->__invoke($valueHoldercad8c, $this, 'getCache', array(), $this->initializerbd045) || 1) && $this->valueHoldercad8c = $valueHoldercad8c;

        return $this->valueHoldercad8c->getCache();
    }

    public function transactional($func)
    {
        $this->initializerbd045 && ($this->initializerbd045->__invoke($valueHoldercad8c, $this, 'transactional', array('func' => $func), $this->initializerbd045) || 1) && $this->valueHoldercad8c = $valueHoldercad8c;

        return $this->valueHoldercad8c->transactional($func);
    }

    public function commit()
    {
        $this->initializerbd045 && ($this->initializerbd045->__invoke($valueHoldercad8c, $this, 'commit', array(), $this->initializerbd045) || 1) && $this->valueHoldercad8c = $valueHoldercad8c;

        return $this->valueHoldercad8c->commit();
    }

    public function rollback()
    {
        $this->initializerbd045 && ($this->initializerbd045->__invoke($valueHoldercad8c, $this, 'rollback', array(), $this->initializerbd045) || 1) && $this->valueHoldercad8c = $valueHoldercad8c;

        return $this->valueHoldercad8c->rollback();
    }

    public function getClassMetadata($className)
    {
        $this->initializerbd045 && ($this->initializerbd045->__invoke($valueHoldercad8c, $this, 'getClassMetadata', array('className' => $className), $this->initializerbd045) || 1) && $this->valueHoldercad8c = $valueHoldercad8c;

        return $this->valueHoldercad8c->getClassMetadata($className);
    }

    public function createQuery($dql = '')
    {
        $this->initializerbd045 && ($this->initializerbd045->__invoke($valueHoldercad8c, $this, 'createQuery', array('dql' => $dql), $this->initializerbd045) || 1) && $this->valueHoldercad8c = $valueHoldercad8c;

        return $this->valueHoldercad8c->createQuery($dql);
    }

    public function createNamedQuery($name)
    {
        $this->initializerbd045 && ($this->initializerbd045->__invoke($valueHoldercad8c, $this, 'createNamedQuery', array('name' => $name), $this->initializerbd045) || 1) && $this->valueHoldercad8c = $valueHoldercad8c;

        return $this->valueHoldercad8c->createNamedQuery($name);
    }

    public function createNativeQuery($sql, \Doctrine\ORM\Query\ResultSetMapping $rsm)
    {
        $this->initializerbd045 && ($this->initializerbd045->__invoke($valueHoldercad8c, $this, 'createNativeQuery', array('sql' => $sql, 'rsm' => $rsm), $this->initializerbd045) || 1) && $this->valueHoldercad8c = $valueHoldercad8c;

        return $this->valueHoldercad8c->createNativeQuery($sql, $rsm);
    }

    public function createNamedNativeQuery($name)
    {
        $this->initializerbd045 && ($this->initializerbd045->__invoke($valueHoldercad8c, $this, 'createNamedNativeQuery', array('name' => $name), $this->initializerbd045) || 1) && $this->valueHoldercad8c = $valueHoldercad8c;

        return $this->valueHoldercad8c->createNamedNativeQuery($name);
    }

    public function createQueryBuilder()
    {
        $this->initializerbd045 && ($this->initializerbd045->__invoke($valueHoldercad8c, $this, 'createQueryBuilder', array(), $this->initializerbd045) || 1) && $this->valueHoldercad8c = $valueHoldercad8c;

        return $this->valueHoldercad8c->createQueryBuilder();
    }

    public function flush($entity = null)
    {
        $this->initializerbd045 && ($this->initializerbd045->__invoke($valueHoldercad8c, $this, 'flush', array('entity' => $entity), $this->initializerbd045) || 1) && $this->valueHoldercad8c = $valueHoldercad8c;

        return $this->valueHoldercad8c->flush($entity);
    }

    public function find($className, $id, $lockMode = null, $lockVersion = null)
    {
        $this->initializerbd045 && ($this->initializerbd045->__invoke($valueHoldercad8c, $this, 'find', array('className' => $className, 'id' => $id, 'lockMode' => $lockMode, 'lockVersion' => $lockVersion), $this->initializerbd045) || 1) && $this->valueHoldercad8c = $valueHoldercad8c;

        return $this->valueHoldercad8c->find($className, $id, $lockMode, $lockVersion);
    }

    public function getReference($entityName, $id)
    {
        $this->initializerbd045 && ($this->initializerbd045->__invoke($valueHoldercad8c, $this, 'getReference', array('entityName' => $entityName, 'id' => $id), $this->initializerbd045) || 1) && $this->valueHoldercad8c = $valueHoldercad8c;

        return $this->valueHoldercad8c->getReference($entityName, $id);
    }

    public function getPartialReference($entityName, $identifier)
    {
        $this->initializerbd045 && ($this->initializerbd045->__invoke($valueHoldercad8c, $this, 'getPartialReference', array('entityName' => $entityName, 'identifier' => $identifier), $this->initializerbd045) || 1) && $this->valueHoldercad8c = $valueHoldercad8c;

        return $this->valueHoldercad8c->getPartialReference($entityName, $identifier);
    }

    public function clear($entityName = null)
    {
        $this->initializerbd045 && ($this->initializerbd045->__invoke($valueHoldercad8c, $this, 'clear', array('entityName' => $entityName), $this->initializerbd045) || 1) && $this->valueHoldercad8c = $valueHoldercad8c;

        return $this->valueHoldercad8c->clear($entityName);
    }

    public function close()
    {
        $this->initializerbd045 && ($this->initializerbd045->__invoke($valueHoldercad8c, $this, 'close', array(), $this->initializerbd045) || 1) && $this->valueHoldercad8c = $valueHoldercad8c;

        return $this->valueHoldercad8c->close();
    }

    public function persist($entity)
    {
        $this->initializerbd045 && ($this->initializerbd045->__invoke($valueHoldercad8c, $this, 'persist', array('entity' => $entity), $this->initializerbd045) || 1) && $this->valueHoldercad8c = $valueHoldercad8c;

        return $this->valueHoldercad8c->persist($entity);
    }

    public function remove($entity)
    {
        $this->initializerbd045 && ($this->initializerbd045->__invoke($valueHoldercad8c, $this, 'remove', array('entity' => $entity), $this->initializerbd045) || 1) && $this->valueHoldercad8c = $valueHoldercad8c;

        return $this->valueHoldercad8c->remove($entity);
    }

    public function refresh($entity)
    {
        $this->initializerbd045 && ($this->initializerbd045->__invoke($valueHoldercad8c, $this, 'refresh', array('entity' => $entity), $this->initializerbd045) || 1) && $this->valueHoldercad8c = $valueHoldercad8c;

        return $this->valueHoldercad8c->refresh($entity);
    }

    public function detach($entity)
    {
        $this->initializerbd045 && ($this->initializerbd045->__invoke($valueHoldercad8c, $this, 'detach', array('entity' => $entity), $this->initializerbd045) || 1) && $this->valueHoldercad8c = $valueHoldercad8c;

        return $this->valueHoldercad8c->detach($entity);
    }

    public function merge($entity)
    {
        $this->initializerbd045 && ($this->initializerbd045->__invoke($valueHoldercad8c, $this, 'merge', array('entity' => $entity), $this->initializerbd045) || 1) && $this->valueHoldercad8c = $valueHoldercad8c;

        return $this->valueHoldercad8c->merge($entity);
    }

    public function copy($entity, $deep = false)
    {
        $this->initializerbd045 && ($this->initializerbd045->__invoke($valueHoldercad8c, $this, 'copy', array('entity' => $entity, 'deep' => $deep), $this->initializerbd045) || 1) && $this->valueHoldercad8c = $valueHoldercad8c;

        return $this->valueHoldercad8c->copy($entity, $deep);
    }

    public function lock($entity, $lockMode, $lockVersion = null)
    {
        $this->initializerbd045 && ($this->initializerbd045->__invoke($valueHoldercad8c, $this, 'lock', array('entity' => $entity, 'lockMode' => $lockMode, 'lockVersion' => $lockVersion), $this->initializerbd045) || 1) && $this->valueHoldercad8c = $valueHoldercad8c;

        return $this->valueHoldercad8c->lock($entity, $lockMode, $lockVersion);
    }

    public function getRepository($entityName)
    {
        $this->initializerbd045 && ($this->initializerbd045->__invoke($valueHoldercad8c, $this, 'getRepository', array('entityName' => $entityName), $this->initializerbd045) || 1) && $this->valueHoldercad8c = $valueHoldercad8c;

        return $this->valueHoldercad8c->getRepository($entityName);
    }

    public function contains($entity)
    {
        $this->initializerbd045 && ($this->initializerbd045->__invoke($valueHoldercad8c, $this, 'contains', array('entity' => $entity), $this->initializerbd045) || 1) && $this->valueHoldercad8c = $valueHoldercad8c;

        return $this->valueHoldercad8c->contains($entity);
    }

    public function getEventManager()
    {
        $this->initializerbd045 && ($this->initializerbd045->__invoke($valueHoldercad8c, $this, 'getEventManager', array(), $this->initializerbd045) || 1) && $this->valueHoldercad8c = $valueHoldercad8c;

        return $this->valueHoldercad8c->getEventManager();
    }

    public function getConfiguration()
    {
        $this->initializerbd045 && ($this->initializerbd045->__invoke($valueHoldercad8c, $this, 'getConfiguration', array(), $this->initializerbd045) || 1) && $this->valueHoldercad8c = $valueHoldercad8c;

        return $this->valueHoldercad8c->getConfiguration();
    }

    public function isOpen()
    {
        $this->initializerbd045 && ($this->initializerbd045->__invoke($valueHoldercad8c, $this, 'isOpen', array(), $this->initializerbd045) || 1) && $this->valueHoldercad8c = $valueHoldercad8c;

        return $this->valueHoldercad8c->isOpen();
    }

    public function getUnitOfWork()
    {
        $this->initializerbd045 && ($this->initializerbd045->__invoke($valueHoldercad8c, $this, 'getUnitOfWork', array(), $this->initializerbd045) || 1) && $this->valueHoldercad8c = $valueHoldercad8c;

        return $this->valueHoldercad8c->getUnitOfWork();
    }

    public function getHydrator($hydrationMode)
    {
        $this->initializerbd045 && ($this->initializerbd045->__invoke($valueHoldercad8c, $this, 'getHydrator', array('hydrationMode' => $hydrationMode), $this->initializerbd045) || 1) && $this->valueHoldercad8c = $valueHoldercad8c;

        return $this->valueHoldercad8c->getHydrator($hydrationMode);
    }

    public function newHydrator($hydrationMode)
    {
        $this->initializerbd045 && ($this->initializerbd045->__invoke($valueHoldercad8c, $this, 'newHydrator', array('hydrationMode' => $hydrationMode), $this->initializerbd045) || 1) && $this->valueHoldercad8c = $valueHoldercad8c;

        return $this->valueHoldercad8c->newHydrator($hydrationMode);
    }

    public function getProxyFactory()
    {
        $this->initializerbd045 && ($this->initializerbd045->__invoke($valueHoldercad8c, $this, 'getProxyFactory', array(), $this->initializerbd045) || 1) && $this->valueHoldercad8c = $valueHoldercad8c;

        return $this->valueHoldercad8c->getProxyFactory();
    }

    public function initializeObject($obj)
    {
        $this->initializerbd045 && ($this->initializerbd045->__invoke($valueHoldercad8c, $this, 'initializeObject', array('obj' => $obj), $this->initializerbd045) || 1) && $this->valueHoldercad8c = $valueHoldercad8c;

        return $this->valueHoldercad8c->initializeObject($obj);
    }

    public function getFilters()
    {
        $this->initializerbd045 && ($this->initializerbd045->__invoke($valueHoldercad8c, $this, 'getFilters', array(), $this->initializerbd045) || 1) && $this->valueHoldercad8c = $valueHoldercad8c;

        return $this->valueHoldercad8c->getFilters();
    }

    public function isFiltersStateClean()
    {
        $this->initializerbd045 && ($this->initializerbd045->__invoke($valueHoldercad8c, $this, 'isFiltersStateClean', array(), $this->initializerbd045) || 1) && $this->valueHoldercad8c = $valueHoldercad8c;

        return $this->valueHoldercad8c->isFiltersStateClean();
    }

    public function hasFilters()
    {
        $this->initializerbd045 && ($this->initializerbd045->__invoke($valueHoldercad8c, $this, 'hasFilters', array(), $this->initializerbd045) || 1) && $this->valueHoldercad8c = $valueHoldercad8c;

        return $this->valueHoldercad8c->hasFilters();
    }

    /**
     * Constructor for lazy initialization
     *
     * @param \Closure|null $initializer
     */
    public static function staticProxyConstructor($initializer)
    {
        static $reflection;

        $reflection = $reflection ?? new \ReflectionClass(__CLASS__);
        $instance   = $reflection->newInstanceWithoutConstructor();

        \Closure::bind(function (\Doctrine\ORM\EntityManager $instance) {
            unset($instance->config, $instance->conn, $instance->metadataFactory, $instance->unitOfWork, $instance->eventManager, $instance->proxyFactory, $instance->repositoryFactory, $instance->expressionBuilder, $instance->closed, $instance->filterCollection, $instance->cache);
        }, $instance, 'Doctrine\\ORM\\EntityManager')->__invoke($instance);

        $instance->initializerbd045 = $initializer;

        return $instance;
    }

    protected function __construct(\Doctrine\DBAL\Connection $conn, \Doctrine\ORM\Configuration $config, \Doctrine\Common\EventManager $eventManager)
    {
        static $reflection;

        if (! $this->valueHoldercad8c) {
            $reflection = $reflection ?? new \ReflectionClass('Doctrine\\ORM\\EntityManager');
            $this->valueHoldercad8c = $reflection->newInstanceWithoutConstructor();
        \Closure::bind(function (\Doctrine\ORM\EntityManager $instance) {
            unset($instance->config, $instance->conn, $instance->metadataFactory, $instance->unitOfWork, $instance->eventManager, $instance->proxyFactory, $instance->repositoryFactory, $instance->expressionBuilder, $instance->closed, $instance->filterCollection, $instance->cache);
        }, $this, 'Doctrine\\ORM\\EntityManager')->__invoke($this);

        }

        $this->valueHoldercad8c->__construct($conn, $config, $eventManager);
    }

    public function & __get($name)
    {
        $this->initializerbd045 && ($this->initializerbd045->__invoke($valueHoldercad8c, $this, '__get', ['name' => $name], $this->initializerbd045) || 1) && $this->valueHoldercad8c = $valueHoldercad8c;

        if (isset(self::$publicPropertiese105a[$name])) {
            return $this->valueHoldercad8c->$name;
        }

        $realInstanceReflection = new \ReflectionClass('Doctrine\\ORM\\EntityManager');

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHoldercad8c;

            $backtrace = debug_backtrace(false, 1);
            trigger_error(
                sprintf(
                    'Undefined property: %s::$%s in %s on line %s',
                    $realInstanceReflection->getName(),
                    $name,
                    $backtrace[0]['file'],
                    $backtrace[0]['line']
                ),
                \E_USER_NOTICE
            );
            return $targetObject->$name;
        }

        $targetObject = $this->valueHoldercad8c;
        $accessor = function & () use ($targetObject, $name) {
            return $targetObject->$name;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();

        return $returnValue;
    }

    public function __set($name, $value)
    {
        $this->initializerbd045 && ($this->initializerbd045->__invoke($valueHoldercad8c, $this, '__set', array('name' => $name, 'value' => $value), $this->initializerbd045) || 1) && $this->valueHoldercad8c = $valueHoldercad8c;

        $realInstanceReflection = new \ReflectionClass('Doctrine\\ORM\\EntityManager');

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHoldercad8c;

            $targetObject->$name = $value;

            return $targetObject->$name;
        }

        $targetObject = $this->valueHoldercad8c;
        $accessor = function & () use ($targetObject, $name, $value) {
            $targetObject->$name = $value;

            return $targetObject->$name;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();

        return $returnValue;
    }

    public function __isset($name)
    {
        $this->initializerbd045 && ($this->initializerbd045->__invoke($valueHoldercad8c, $this, '__isset', array('name' => $name), $this->initializerbd045) || 1) && $this->valueHoldercad8c = $valueHoldercad8c;

        $realInstanceReflection = new \ReflectionClass('Doctrine\\ORM\\EntityManager');

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHoldercad8c;

            return isset($targetObject->$name);
        }

        $targetObject = $this->valueHoldercad8c;
        $accessor = function () use ($targetObject, $name) {
            return isset($targetObject->$name);
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = $accessor();

        return $returnValue;
    }

    public function __unset($name)
    {
        $this->initializerbd045 && ($this->initializerbd045->__invoke($valueHoldercad8c, $this, '__unset', array('name' => $name), $this->initializerbd045) || 1) && $this->valueHoldercad8c = $valueHoldercad8c;

        $realInstanceReflection = new \ReflectionClass('Doctrine\\ORM\\EntityManager');

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHoldercad8c;

            unset($targetObject->$name);

            return;
        }

        $targetObject = $this->valueHoldercad8c;
        $accessor = function () use ($targetObject, $name) {
            unset($targetObject->$name);

            return;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $accessor();
    }

    public function __clone()
    {
        $this->initializerbd045 && ($this->initializerbd045->__invoke($valueHoldercad8c, $this, '__clone', array(), $this->initializerbd045) || 1) && $this->valueHoldercad8c = $valueHoldercad8c;

        $this->valueHoldercad8c = clone $this->valueHoldercad8c;
    }

    public function __sleep()
    {
        $this->initializerbd045 && ($this->initializerbd045->__invoke($valueHoldercad8c, $this, '__sleep', array(), $this->initializerbd045) || 1) && $this->valueHoldercad8c = $valueHoldercad8c;

        return array('valueHoldercad8c');
    }

    public function __wakeup()
    {
        \Closure::bind(function (\Doctrine\ORM\EntityManager $instance) {
            unset($instance->config, $instance->conn, $instance->metadataFactory, $instance->unitOfWork, $instance->eventManager, $instance->proxyFactory, $instance->repositoryFactory, $instance->expressionBuilder, $instance->closed, $instance->filterCollection, $instance->cache);
        }, $this, 'Doctrine\\ORM\\EntityManager')->__invoke($this);
    }

    public function setProxyInitializer(\Closure $initializer = null) : void
    {
        $this->initializerbd045 = $initializer;
    }

    public function getProxyInitializer() : ?\Closure
    {
        return $this->initializerbd045;
    }

    public function initializeProxy() : bool
    {
        return $this->initializerbd045 && ($this->initializerbd045->__invoke($valueHoldercad8c, $this, 'initializeProxy', array(), $this->initializerbd045) || 1) && $this->valueHoldercad8c = $valueHoldercad8c;
    }

    public function isProxyInitialized() : bool
    {
        return null !== $this->valueHoldercad8c;
    }

    public function getWrappedValueHolderValue() : ?object
    {
        return $this->valueHoldercad8c;
    }
}
